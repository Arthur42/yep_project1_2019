<?php
function randomString($size, $listChar = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
{
    $chaine = '';
    $max = mb_strlen($listChar, '8bit') - 1;
    for ($i = 0; $i < $size; ++$i) {
        $chaine .= $listChar[random_int(0, $max)];
    }
    return $chaine;
}
?>