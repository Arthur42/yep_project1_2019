<html>
    <?php include "php/head.php" ?>
    <script src="js/password_verification.js"></script>
    <?php
        $error = '';
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $password = $_POST['password'];
            $password_hashed = password_hash($password, PASSWORD_DEFAULT);
            if ($bdd->insert($_POST['name'], $_POST['surname'], $_POST['email'], $password_hashed)) {
                echo '<script> window.location.href = \'email_verification\' </script>';
            } else {
                $error = _registerError;
            }
        }
    ?>
    </head>
    <body>
        <?php include "php/header.php" ?>
        <h1><?php echo _registerAccount; ?></h1>
        <form action="register" method="POST" id="form">
            <label for="name"> <?php echo _lastName; ?>: </label>
            <input type="text" placeholder="<?php echo _lastName; ?>" lenght="255" name="name" required id="name" size='50'/><br />
            <label for="surname"> <?php echo _firstName; ?>: </label>
            <input type="text" placeholder="<?php echo _firstName; ?>" lenght="255" name="surname" required id="surname" size='50'/><br />
            <label for="email"> <?php echo _loginEmail; ?>: </label>
            <input type="email" placeholder="<?php echo _loginEmail; ?>" lenght="255" name="email" required id="email" size='50'/><br />
            <label for="password"> <?php echo _password; ?>: </label>
            <input type="password" placeholder="<?php echo _password; ?>" lenght="255" name="password" required id="password" size='50'/><br />
            <label for="password_check"><?php echo _passwordVerification; ?>: </label>
            <input type="password" placeholder="<?php echo _passwordVerification; ?>" lenght="255" name="password_check" required id="password_check" size='50'/><br />
            <input type="submit" value="<?php echo _accountCreation; ?>" />
        </form>
        <?php
        if ($error != '')
            echo '<br />' . $error . '<br />';
        ?>
        <?php include "php/footer.php" ?>
        </footer>
    </body>
</html>