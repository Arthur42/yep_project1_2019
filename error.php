<html>
    <?php include "php/head.php" ?>
    <?php
    $error_title = _errorDefaultTitle;
    $error_message = _errorDefaultMessage;
if (isset($_GET['e'])) {
    switch ($_GET['e']) {
        case 400:
            $error_title = _error400Title;
        break;
        case 401:
            $error_title = _error401Title;
        break;
        case 403:
            $error_title = _error403Title;
        break;
        case 404:
            $error_title = _error404Title;
            $error_message = _error404Message;
            if (isset($_SESSION['user']))
                $error_message = _error404MessageNotLogged;
        break;
        case 500:
            $error_title = _error500Title;
        break;
        default:
    }
}
?>
    </head>
    <body>
        <?php include "php/header.php" ?>
        <div class="error-page big-text quiet">
            <h1 class="error-page-message"><?php echo $error_title; ?></h1>
            <p class="error-page-message"><?php echo $error_message; ?></p>
            <?php
            if (isset($_SESSION['user'])) {
                echo '<p class="error-page-message">' . _errorCheckIdentity . '</p>';
            }
            ?>
        </div>
        <?php include "php/footer.php" ?>
        </footer>
    </body>
</html>