<html>
    <?php include 'php/head.php' ?>
    <?php include 'php/session.php' ?>
    <?php $user = $bdd->getUserByEmail($_SESSION['user']); ?>
    <?php
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if ($bdd->updateInformations($_POST['name'], $_POST['surname'], $_POST['pseudo'], $user->getEmail())) {
                echo '<script>window.location.href=\'/yep_project1_2019/user_setting\'</script>';
            } else {
                $error = _errorWhileUpdatingInformation;
            }
        }
    ?>
    </head>
    <body>
        <?php include "php/header.php" ?>
        <h1><?php echo _changeInformation; ?></h1>
        <form action='change_information' method='POST'>
            <label for='name'><?php echo _lastName; ?>: </label>
            <input name='name' id='name' value='<?php echo $user->getName(); ?>' placeholder='<?php echo _lastName; ?>' /><br />
            <label for='surname'><?php echo _firstName; ?>: </label>
            <input name='surname' id='surname' value='<?php echo $user->getSurname(); ?>' placeholder='<?php echo _firstName; ?>' /><br />
            <label for='pseudo'><?php echo _pseudo; ?>: </label>
            <input name='pseudo' id='pseudo' value='<?php echo $user->getPseudo(); ?>' placeholder='Pseudonyme' /><br />
            <input type='submit' value='<?php echo _confirm; ?>' />
        </form>
        <br />
        <a href='/yep_project1_2019/user_setting'><?php echo _cancel; ?></a>
        <?php
            if (isset($error))
                echo '<p style=\'color: red\'>' . $error . '</p>';
        ?>
        <?php include "php/footer.php" ?>
        </footer>
    </body>
</html>
