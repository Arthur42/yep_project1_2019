<?php
    session_start();
    include __DIR__ . '/../database/database.php';
    $bdd = new Database;
    include __DIR__ . '/../lang/lang.php';
?>
<head>
    <link rel="shortcut icon" type="image/x-icon" href="/yep_project1_2019/img/favicon.ico" />
    <link rel="stylesheet" href="/yep_project1_2019/css/style.css" type="text/css" />
    <link rel="stylesheet" href="/yep_project1_2019/css/header.css" type="text/css" />
    <link href="https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fuse.js@5.0.10-beta"></script>