<?php
    if (!isset($_SESSION['user'])) {
        echo '<script>window.location.href=\'/yep_project1_2019/login\'</script>';
    } else {
        $email = $_SESSION['user'];
        if (!$bdd->isVerified($email))
            echo '<script>window.location.href=\'/yep_project1_2019/email_verification\'</script>';
    }
?>