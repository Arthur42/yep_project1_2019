<script>
  const personList = [
      <?php
        foreach ($bdd->getUsers() as $key => $row) {
            if ($key > 0)
                echo ',';
            echo '{name: "@' . $row['PSEUDO'] . '",';
            echo 'link: "/yep_project1_2019/user?name=' . strtolower($row['PSEUDO']) . '"}';
        }
      ?>
]
const boardList = [
    <?php
        if (isset($_SESSION['user'])) {
            foreach ($bdd->getUserBoards($_SESSION['user']) as $key => $row) {
                if ($key > 0)
                    echo ',';
                echo '{name: "' . $row . '",';
                echo 'link: "/yep_project1_2019/board?board=' . $row . '"}';
            }
        }
      ?>
]
const cardList = [
    <?php
        if (isset($_SESSION['user'])) {
            foreach ($bdd->getUserTicketList($_SESSION['user']) as $key => $row) {
                if ($key > 0)
                    echo ',';
                echo '{name: "@' . $row['ticketName'] . '",';
                echo 'link: "/yep_project1_2019/board?board=' . $row['boardName'] . '"}';
            }
        }
      ?>
]
</script>
<div class="popup-create-board-content-out popup-hidden">
    <div class="popup-create-board-content-in">
        <form action="/yep_project1_2019/board" method="POST">
            <label><?php echo _boardName ?> :</label>
            <input type="text" name="boardName">
            <input type="submit" value="create">
        </form>
    </div>
</div>
<header>
    <div id="header-in">
        <div class="header-left">
            <div class="header-button-cell">
                <div class="header-cell">
                    <a href="/yep_project1_2019/logout"><i class="icon-signin"></i></a>
                </div>
            </div>
            <div class="header-button-cell">
                <div class="header-cell">
                    <a href="/yep_project1_2019/index"><i class="icon-home"></i></a>
                </div>
            </div>
            <div class="header-button-cell">
                <div class="header-cell popup-container popup-button">
                    <div><?php echo _boards; ?></div>
                </div>
                    <div class="popup popup-hidden">
                        <div class="popup-content">
                            <input type="text" placeholder="<?php echo _search; ?>" id="header-board-search-bar" class="transparent">
                        </div>
                        <div class="popup-content-list">
                            <div id="header-board-search-content"></div>
                        </div>
                        <div class="popup-create-board">
                            <div class="popup-content"><?php echo _createBoard; ?></div>
                        </div>
                    </div>
            </div>
            <div class="header-button-cell">
                <div class="header-cell popup-container">
                    <input type="text" placeholder="<?php echo _search; ?>" id="header-search-bar" class="popup-button transparent">
                    <div class="popup popup-large popup-hidden">
                        <div class="popup-content">
                            <div><b id="header-search-title"></b></div>
                            <div id="header-search-content"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div><div class="header-center">
            <div class="header-cell">
                <a href="/yep_project1_2019/">
                    <img class="header-logo" src="/yep_project1_2019/img/logo.png" />
                    <span>Dyscovery Trello</span>
                </a>
            </div>
        </div><div class="header-right">
        <div class="right">
            <div class="header-button-cell">
                <div class="header-cell">
                    <a href='/yep_project1_2019/user_setting/'><i class='icon-gear'></i></a>
                </div>
            </div>
            <div class="header-button-cell">
                <div class="header-cell popup-button popup-container">
                    <i class="icon-bell"></i>
                </div>
                <div class="popup popup-open-left popup-hidden">
                    <div class="popup-header">
                        <div class="popup-title"><h1><?php echo _notification; ?></h1></div>
                        <div class="popup-close">
                            <div class="popup-close-icon">
                                <i class="icon-angle-up"></i>
                            </div>
                        </div>
                    </div>
                    <div class="popup-content">
                    </div>
                </div>
            </div>
            <div class="header-button-cell">
                <div class="header-cell popup-button popup-container">
                    <i class="icon-info"></i>
                </div>
                <div class="popup popup-open-left popup-hidden">
                    <div class="popup-header">
                        <div class="popup-title"><h1><?php echo _information; ?></h1></div>
                        <div class="popup-close">
                            <div class="popup-close-icon">
                                <i class="icon-angle-up"></i>
                            </div>
                        </div>
                    </div>
                    <div class="popup-content">
                        <a href="/yep_project1_2019/privacy_policy"><p><?php echo _confidentiality; ?></p></a>
                    </div>
                    <div class="popup-content">
                        <a href="/yep_project1_2019/our_developpers"><p><?php echo _developers; ?></p></a>
                    </div>
                    <div class="popup-content">
                        <a href="/yep_project1_2019/help.php"><p><?php echo _help; ?></p></a>
                    </div>
                    <div class="popup-content">
                        <a href="/yep_project1_2019/application"><p><?php echo _application; ?></p></a>
                    </div>
                    <div class="popup-content">
                        <a href="/yep_project1_2019/cloud_terms_of_service"><p><?php echo _legal; ?></p></a>
                    </div>
                </div>
            </div>
            <div class="header-button-cell">
                <div class="header-cell popup-button popup-container">
                    <i class="icon-plus"></i>
                </div>
                <div class="popup popup-open-left popup-hidden">
                    <div class="popup-header">
                        <div class="popup-title"><h1><?php echo _create; ?></h1></div>
                        <div class="popup-close">
                            <div class="popup-close-icon">
                                <i class="icon-angle-up"></i>
                            </div>
                        </div>
                    </div>
                    <a href="#" class="popup-create-board">
                        <div class="popup-content">
                            <h2><?php echo _createBoard; ?></h2>
                            <p><?php echo _createBoardText; ?></p>
                        </div>
                    </a>
                    <a href="#">
                        <div class="popup-content">
                            <h2><?php echo _createTeam; ?></h2>
                            <p><?php echo _createTeamText; ?></p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        </div>
    </div>
</header>
