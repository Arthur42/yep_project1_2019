<?php
if (isset($_GET["board"]) && $bdd->boardExist($_GET["board"], $_SESSION['user'])) {
    $board_column = $bdd->getBoardColumn($_GET["board"], $_SESSION['user']);
    $board_content = $bdd->getBoardContent($_GET["board"], $_SESSION['user']);
} else {
    echo '<script>window.location.href=\'/yep_project1_2019/error\'</script>';
}
?>
<div class="board-content">
    <div class="board-content-column" id="sortable"></div>
    <div class="board-content-next"></div>
</div>
<script>
    var board_content = <?php echo ((isset($board_content) && empty($board_content) === false) ? $board_content : '{}'); ?>;
    var board_column = <?php echo ((isset($board_column) && empty($board_column) === false) ? $board_column : '{}'); ?>;
    $(document).ready(function(){
        
        // display add column
        
        $(".board-content-next").append('<div class="board-column"><form action="board?board=<?php echo $_GET["board"]; ?>" method="POST"><input type="text" name="addColumn"><input type="submit" value="+ add column"></form></div>');
        
        // update data every 1 second
        
        setInterval(() => {
            var jqxhr = $.get( "boardUpdate.php?board=<?php echo $_GET["board"]; ?>", function(data, status) {
                data = JSON.parse(data);
                board_content = data["content"];
                board_column = data["column"];
                updateColumn();
                updateContent();
            })
            .fail(function() {
                alert( "error" );
            });
        }, 1000);
        
        // update data at page initialisation
        
        updateColumn();
        updateContent();
    });
    
        
    // display column
    
    function updateColumn() {
        $.each(board_column, function(i, item) {
            if (item["archive"] != "0") {
                $('#column-' + item["id"]).remove();
            } else if (!$('#column-' + item["id"]).length)
            $(".board-content-column").append('\
                <div class="board-column" id="column-' + item["id"] + '">\
                   <div class="column-drag"></div>\
                   <div class="column-title">\
                        <h2>' + item["name"] + '</h2>\
                        <div>\
                        <div class="popup-button popup-container">\
                                <i class="icon-ellipsis-horizontal"></i>\
                            </div>\
                            <div class="popup popup-open-left popup-hidden">\
                                <div class="popup-header">\
                                    <div class="popup-title"><h1>Action List</h1></div>\
                                    <div class="popup-close">\
                                        <div class="popup-close-icon">\
                                            <i class="icon-angle-up"></i>\
                                        </div>\
                                    </div>\
                                </div>\
                                <a href="#" onClick="ArchiveColumn(\'' + item["id"] + '\')">\
                                    <div class="popup-content">\
                                        <p>Archive board</p>\
                                    </div>\
                                </a>\
                            </div>\
                        </div>\
                    </div>\
                    <div>\
                        <a href=\'#\' class=\'ticket_create\'>Ajouter un ticket</a>\
                    </div>\
                </div>');
        });
    }
    
    // display ticket
    
    function updateContent() {
        $('[id^="column-"]').each(function(i, e){
            let d = $(e).attr('id');
            $('#' + d + ' .ticket').remove();
        });
        $.each(board_content, function(i, item) {
            if ($('#column-' + item["columnID"]).length) {
                $("#column-" + item["columnID"] + ' .column-title').after('\
                <a href=\'' + item["columnID"] + '/' + item["ticketID"] + '\' class=\'ticket\'>' + item["title"] + '</a>');
            }
        });
    }
    
    // archive column
    
    function ArchiveColumn(column) {
        var arg = "&archiveColumn=" + column;
        $.get( "boardUpdate.php?board=<?php echo $_GET["board"]; ?>" + arg, function(data, status) {
            data = JSON.parse(data);
            board_content = data["content"];
            board_column = data["column"];
            updateColumn();
            updateContent();
        })
        .fail(function() {
            alert( "error" );
        });
    }
    
    // list movement
    
    $(function() {
        $("#sortable").sortable({
            handle: ".column-drag",
            start: function (event, ui) {
                var pos = ui.item.index();
                ui.item.data('start_pos', pos);
            },
            update:  function (event, ui) {
                var start_pos = ui.item.data('start_pos');
                var pos = ui.item.index();
                updateColumnPosition(start_pos, pos);
            }
        });
    });
    
    // change list position
    
    function updateColumnPosition(oldPos, newPos) {
        var arg = "&oldColumnPos=" + (oldPos + 1) + "&newColumnPos=" + (newPos + 1);
        console.log("boardUpdate.php?board=<?php echo $_GET["board"]; ?>" + arg);
        $.get( "boardUpdate.php?board=<?php echo $_GET["board"]; ?>" + arg, function(data, status) {
            data = JSON.parse(data);
            board_content = data["content"];
            board_column = data["column"];
            updateColumn();
            updateContent();
        })
        .fail(function() {
            alert( "error" );
        });
    }
    
</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>