<link rel="stylesheet" href="/yep_project1_2019/css/footer.css" type="text/css" />

<?php
if (isset($_GET['accept-cookies'])) {
	setcookie('accept-cookies', 'true', time() + 365*24*3600);
	header('Location: ./');
}
?>

<footer>
	<body>
		<?php
			if (!isset($_COOKIE['accept-cookies'])) {
		?>
			<div class="cookie-banner">
				<div class="container">
					<p><?php echo _cookieTextLeft; ?><a href="/cookies"><?php echo _cookieTextLink; ?></a><?php echo _cookieTextRight; ?></p>
					<a href="?accept-cookies" class="button"><?php echo _cookieContinue; ?></a>
				</div>
			</div>
		<?php
			}
		?>
	</body>
</footer>

<script src="/yep_project1_2019/js/header.js" type="text/javascript"></script>
<script src="/yep_project1_2019/js/popup.js" type="text/javascript"></script>