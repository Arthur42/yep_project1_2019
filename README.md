# yep_projet1_2019

## MAIL
Pour pourvoir envoyer des emails sous WAMP il faut suivre les étapes suivantes:
 - Télécharger sendmail à l'adresse suivante: http://www.glob.com.au/sendmail/sendmail.zip
 - Créer un dossier dans Wamp, exemple: C:\wamp64\sendmail
 - Décompresser le zip dans ce dossier
 - Modifier le ficher sendmail.ini avec les informations suivantes:
    - smtp_server=smtp-mail.Outlook.com
    - smtp_port=587
    - debug_logfile=debug.log
    - auth_username= "VOTRE ADRESSE MAIL"
    - auth_password= "VOTRE MOT DE PASSE DE LA BOITE MAIL SPECIFIER"
- Ouvrer le fichier php.ini en cliquant gauche sur l'icône WAMP puis PHP > php.ini
- Modifier la ligne suivant:
    - sendmail_path = "\<Path du dossier sendmail>\sendmail.exe -t -i"

Le serveur mail est prêt il faut maintenant créer le fichier /usefull/setting.php est coller le code suivant en indiquant votre adresse mail :
```php
<?php
    $from = 'Votre adresse mail';
?>
```

