<html>
    <?php include "php/head.php" ?>
    <?php
    $error = '';
    if (!isset($_SESSION['user'])) {
        echo '<script>window.location.href=\'login\'</script>';
    } else if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $email = $_SESSION['user'];
        $code = $_POST['code'];

        if ($bdd->codeVerification($email, $code)) {
            $bdd->updateVerificationChecked($email);
            echo '<script>window.location.href=\'index\'</script>';
        } else {
            $error = _emailVerifError;
        }
    } else {
        $email = $_SESSION['user'];
        if ($bdd->isVerified($email)) {
            echo '<script>window.location.href=\'index\'</script>';
        } else {
            include 'usefull/random_string.php';
            include 'usefull/mail.php';

            $code = randomString(5);
            $bdd->updateVerificationCode($email, password_hash($code, PASSWORD_DEFAULT));
            $msg = _yourVerificationCodeIs . $code;
            $mail = new Mail;
            $mail->sendMail($email, _verificationCode, $msg);
        }
    }
    ?>
    </head>
    <body>
        <?php include "php/header.php" ?>
        <h1><?php echo _emailVerification; ?></h1>
        <p><?php echo _emailVerifCode; ?></p>
        <form action='email_verification' method='POST'>
            <label for='code'><?php echo _code; ?>: </label>
            <input type='text' lenght='5' size='5' name='code' id='code' />
            <input type='submit' value='<?php echo _check; ?>' />
        </form>
        <?php
            if ($error != '')
                echo '<br /><p style\'color: red\'>' . $error . '</p><br />';
        ?>
        <?php include "php/footer.php" ?>
        </footer>
    </body>
</html>