<html>
    <?php include "php/head.php" ?>
    <?php
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            echo '<script> window.location.href = \'index\' </script>';
        }
    ?>
    </head>
    <body>
        <?php include "php/header.php" ?>
        <h1><?php echo _changePassword; ?></h1>
        <?php
        $email = $_POST['email'];
        $password = $_POST['password'];
        $password_check = $_POST['password_check'];
        $code = $_POST['code'];
        
        if ($bdd->codeVerification($email, $code)) {
            $bdd->updatePassword($email, $password);
            ?>
            <p><?php echo _passwordChanged; ?></p><br />
            <a href='login'><?php echo _login; ?></a>
            <?php
        } else {
            ?>
            <p style='color:red'><?php echo _codeError; ?></p>
            <a href='recovery'><?php echo _newTry; ?></a>
            <?php
        }
    ?>
        <?php include "php/footer.php" ?>
        </footer>
    </body>
</html>