<link rel="stylesheet" href="/yep_project1_2019/css/help.css" type="text/css"/>

<html>
    <?php include __DIR__ . "/../php/head.php" ?>
    </head>
    <body>
        <?php include __DIR__ . "/../php/header.php" ?>
        <section class="main-content">
            <div class="contentWrapper">
                <h1>Utilisation du Power-Up du calendrier</h1>
                <p>Avec la mise sous tension du calendrier activée, vous pouvez afficher les cartes de votre forum avec les dates d'échéance dans un calendrier. Vous pouvez également activer un flux iCalendar que les applications tierces peuvent regarder.</p>
                <h2>Affichage du calendrier</h2>
                <p> Cliquez sur le lien Calendrier en haut à droite du tableau pour passer en vue calendrier. </p>
                <p><img src="../img/display_calendar" style="display: block; margin: auto; width: 362px;"></p>
                <p> Certaines choses que vous pouvez faire avec l'agenda de démarrage:</p>
                <ul>
                    <li>
                        <p>Passez rapidement du mode semaine au mode mois pour un affichage plus large ou plus spécifique des cartes</p>
                    </li>
                    <li>
                        <p>Faites glisser les cartes entre les jours du calendrier pour changer rapidement leur date limite</p>
                    </li>
                    <li>
                        <p>Cliquez pour développer les jours du calendrier pour un affichage des cartes plus spacieux</p>
                    </li>
                </ul>
                <h2>Activer le flux iCalendar</h2>
                <p> Les calendriers tiers peuvent importer des flux iCalendar (iCal), ce qui vous permet d'afficher les cartes Trello de votre forum qui ont des dates d'échéance dans d'autres calendriers. Pour accéder au flux iCalendar, ouvrez le menu de la carte, cliquez sur "Power-Ups", puis cliquez sur l'engrenage des outils dans la section "Calendrier". Ensuite, cliquez sur Activer la synchronisation.</p>
                <p><img src="../img/syncro_calendar" style="width: 604px; display: block; margin: auto;"></p>
                <p> Vous pouvez maintenant copier l'URL depuis "Votre flux personnel iCalendar".</p>
                <p><img src="../img/calendar_feed" style="display: block; margin: auto; width: 100%; max-width: 350px;"></p>
                <p> Le flux iCal pour un forum est généré individuellement pour chaque membre du conseil, mais si plusieurs personnes regardent un seul lien, ils verront tous le calendrier en question. Bien que la vue du calendrier soit la même pour le flux de chaque membre, la raison pour laquelle nous en générons une nouvelle pour chaque membre est que si quelqu'un est retiré d'un forum, nous pouvons également désactiver son accès au calendrier de ce forum.</p>
                <h2>Vue du calendrier unique</h2>
                <p> Si vous souhaitez afficher tous les calendriers de vos forums dans un seul calendrier, vous pouvez combiner plusieurs flux iCal dans une seule vue de calendrier sur le calendrier externe. Pour ce faire, activez le flux iCal sur chaque carte et ajoutez chaque flux iCal à votre calendrier externe. </p>
                <p> Si vous ne voyez pas l'option de création d'un flux iCalendar, il est possible que ce paramètre ne soit pas activé sur le tableau. Un administrateur du tableau peut activer le flux iCalendar en cliquant sur l'icône d'engrenage à côté de Power-Up du calendrier, et en cliquant sur « Activé » pour « Activer le flux iCalendar. »</p>
            </div>
        </section>
        <?php include __DIR__ . "/../php/footer.php" ?>
        </footer>
    </body>
</html>