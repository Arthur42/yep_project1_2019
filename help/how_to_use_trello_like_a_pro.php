<link rel="stylesheet" href="/yep_project1_2019/css/help.css" type="text/css"/>

<html>
    <?php include __DIR__ . "/../php/head.php" ?>
    </head>
    <body>
        <?php include __DIR__ . "/../php/header.php" ?>
        <section class="main-content">
            <div class="contentWrapper">
                <h1>Comment utiliser Trello comme un pro</h1>
                <p> Le Guide d'aide est plein d'informations utiles en ce qui concerne l'utilisation Trello, mais nous avons réuni certains de nos trucs et astuces préférées ici, ainsi que les articles dont ils sont issus. Ceux - ci sont très utiles si vous êtes déjà familier avec la façon d'utiliser Trello-si vous êtes à la recherche d'informations de base, consultez le en route.</p>
                <h3>Double-cliquez pour ajouter la liste</h3>
                <p> Vous pouvez double-cliquer sur un espace libre sur une tableau pour ouvrir la liste des add pop-over. Trello est assez intelligent pour savoir où vous avez cliqué, et par défaut à cet emplacement.</p>
                <h3>Raccourcis clavier</h3>
                <p> Allez à https://trello.com/shortcuts ou appuyez sur "?" Sur votre clavier tout en Trello pour afficher une liste des raccourcis clavier.</p>
                <h3>Faites glisser et déposer des pièces jointes</h3>
                <p> Vous pouvez glisser et déposer plusieurs fichiers de votre ordinateur vers une carte pour les télécharger. Vous pouvez également faire glisser des images provenant d'autres sites à vos cartes.</p>
                <h3>Proposez plusieurs cartes et des éléments de la liste de contrôle</h3>
                <p> Vous pouvez ajouter plusieurs cartes ou de listes à la fois. Copier et coller à partir d'une colonne de feuille de calcul ou d'une liste séparée par de nouvelles lignes à partir d'un traitement de texte. Cela va créer une nouvelle carte ou liste de contrôle pour chaque ligne de la liste. Ce ne sera pas interférer avec le rapide et facile presse-enter-to-create-a-card méthode; il ne fonctionnera que lors du collage de texte.</p>
                <h3>@ mentionne</h3>
                <p> Besoin d'attirer l'attention d'un autre membre du tableau? Vous pouvez les mentionner dans un commentaire de la carte et il va générer une notification pour eux. Il est obtenu autocomplete, aussi. Il suffit de commencer à taper «@» et leur nom et vous obtenez des suggestions.</p>
                <h3>tableau Faites glisser pour faire défiler</h3>
                <p> Si vous cliquez et maintenez sur le fond d'un tableau, vous pouvez faire glisser la tableau autour de faire défiler horizontalement. Vous pouvez également faire défiler horizontalement en appuyant sur "shift" et puis en faisant défiler avec votre souris.</p>
                <h3>Re-order joué tableaux</h3>
                <p> Vous pouvez réorganiser vos tableaux étoilés en les faisant glisser. De cette façon, vous pouvez mettre votre tableau le plus important au sommet de la liste, ce qui rend encore plus facile d'accès.</p>
                <h3>Réorganiser listes de contrôle et les éléments de la liste de contrôle</h3>
                <p> Cliquez et maintenez le titre du liste de contrôle ou un élément de liste de contrôle pour faire glisser autour. Vous pouvez réorganiser les listes de contrôle, déplacer des éléments vers le haut, et même déplacer des éléments entre les listes de contrôle.</p>
                <h3>Créer des cartes par e-mail</h3>
                <p> Chaque tableau a une adresse e-mail unique, vous pouvez envoyer un mail pour créer des cartes. Adresse en allant dans le menu du tableau, puis en cliquant sur \Paramètres Email-à-bord."</p>
                <h3>Afficher cartes totales par liste</h3>
                <p> Vous pouvez filtrer les cartes sur une tableau en appuyant sur 'f'- essayer de filtrage pour "*" ou "/", sans autre texte, et vous verrez le nombre de cartes par liste.</p>
                <h3>Copiez tableaux, des listes, des cartes et des listes de contrôle</h3>
                <p> Vous avez un tableau ou une carte que vous voulez utiliser comme modèle? Vous pouvez copier une carte, une liste ou une carte d'origine dans un nouvel emplacement au lieu de créer une carte vide. Vous pouvez également copier des éléments de la liste de contrôle à partir d'autres listes de contrôle sur votre tableau lors de la création d'une nouvelle liste de contrôle. Cliquez simplement sur le bouton "Ajouter une liste de contrôle ..." dans la barre latérale de la carte et sélectionnez une option dans "Copier les éléments de ...".</p>
                <h3>tableau Trello Ressources</h3>
                <p> Il y a des tonnes d'exemples d'utilisation, des extensions, bookmarklets et plus sur le administration Trello Ressources. Vérifiez-le! Nous vous tiendrons à jour que nous trouvons de nouvelles choses.</p>
                <h3>actions de la liste en vrac</h3>
                <p> Vous pouvez déplacer ou archiver toutes les cartes dans une liste à la fois (comme l'archivage tout dans votre liste "Done"). Survolez le titre de la liste, cliquez sur le bouton de menu dans le coin supérieur droit, et sélectionnez "Déplacer toutes les cartes dans cette liste ...» ou «Archives toutes les cartes dans cette liste ...».</p>
                <h3>notifications sur le bureau</h3>
                <p> Si vous utilisez Chrome ou Safari, votre navigateur peut générer des notifications de bureau, un standard Web émergents. Il suffit de cliquer sur le bouton Notifications dans l'en-tête, et sélectionnez "Autoriser les notifications de bureau ..." dans le menu.</p>
                <h3>Réorganiser et renommer les pièces jointes</h3>
                <p> Après avoir ajouté des pièces jointes à une carte, vous pouvez renommer la pièce jointe en cliquant sur le texte du nom de fichier. Cela vous permettra d'éditer le <em>nom d'affichage</em> de la pièce jointe. Notez que le nom de fichier actuel n'aura pas été modifié.</p>
                <p> Si vous souhaitez réorganiser les pièces jointes, cliquez et maintenez la pression, puis faites glisser la pièce jointe jusqu'à la position souhaitée.</p>
                <h3>lectures complémentaires</h3>
                <p> Plusieurs membres de l'équipe Trello ont écrit des articles sur notre blog pour aider à prendre votre utilisation de Trello au niveau suivant:</p>
                <ul>
                    <li>
                        <p>Comment utiliser Trello comme un pro : Il couvre les choses cool comme les raccourcis clavier, un double-clic pour ajouter des listes, la magie avec des pièces jointes, tout copier, et plus encore.</p>
                    </li>
                    <li>
                        <p> de Trello comme un Pro: Partie 2 : Nouveaux raccourcis, copier / coller pour faire plusieurs cartes, et mises à jour de la liste de contrôle.</p>
                    </li>
                    <li>
                        <p> de Trello comme un Pro: Partie 3 : Power-ups, créer des cartes par courriel, Trello or et classe affaires, société de milieux, des raccourcis, réordonner favoris tableaux</p>
                    </li>
                    <li>
                        <p> de tableaux multiples pour un super-flexible workflow : Est -ce qu'un seul tableau obtenir longtemps dans la dent? Utilisez la capacité de Trello à déplacer les cartes entre les tableaux pour améliorer votre flux de travail.</p>
                    </li>
                    <li>
                        <p>Communauté de Trello : elle est composée d'experts et d'utilisateurs de Trello, une excellente façon d'obtenir une réponse rapidement - parfait pour les pratiques, les questions d'affaire ou les conseils!</p>
                    </li>
                </ul>
            </div>
        </section>
        <?php include __DIR__ . "/../php/footer.php" ?>
        </footer>
    </body>
</html>