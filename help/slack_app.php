<link rel="stylesheet" href="/yep_project1_2019/css/help.css" type="text/css"/>

<html>
    <?php include __DIR__ . "/../php/head.php" ?>
    </head>
    <body>
        <?php include __DIR__ . "/../php/header.php" ?>
        <section class="main-content">
            <div class="contentWrapper">
                <h1>L'application Trello pour Slack</h1>
                <p> L'application Trello pour Slack associe vos équipes Trello à vos équipes Slack : Présentation de l'application Trello pour Slack .</p>
                <p> Pour faciliter la collaboration, tout membre Slack n'appartenant pas à l'équipe Trello pourra rejoindre l'équipe Trello associée. Cela peut être contrôlé par une configuration de l'équipe Trello, si votre équipe a souscrit à Business Class.</p>
                <p> Pour obtenir des instructions sur la façon d'installer l'application Slack, lisez Connexion de l'application Trello pour Slack .</p>
                <p> Trello pour Slack est actuellement disponible en anglais uniquement, car Slack ne prend en charge que l'anglais. Pour avoir une explication des permissions d'accès de Slack que nous demandons, lisez Permissions d'accès demandé pour l'application Trello pour Slack .</p>
                <h2>Bot</h2>
                <p> Bot. Lorsqu'un lien vers une carte ou un tableau visible d'une équipe est envoyé vers une chaîne dans laquelle le bot @trello est, les détails s'afficheront automatiquement et cela vous permettra d'effectuer des actions rapides. Tout le monde dans la chaîne Slack pourra voir le tableau ou la carte, même sans avoir de compte Trello.</p>
                <p><img src="../img/bot_slack" style="width: 508px;"></p>
                <p> @trello ne rejoindra automatiquement aucune chaîne de lui-même. Invitez @trello sur les chaînes où vous souhaitez déployer des liens Trello en tapant « /invite @trello » sans les guillemets sur ces chaînes.</p>
                <p> Remarque : pour des raisons de sécurité, @trello n'affichera que les détails des cartes et des tableaux qui se trouvent dans l'équipe Trello associée, et qui sont visibles par l'équipe (c.-à-d. que @trello ne déploiera pas le contenu sur les tableaux qui sont privés). Pour déployer le contenu sur les tableaux privés, vous pouvez utiliser la commande <code>/trello url-to-card-or-board</code>.</p>
                <h2>Recevoir une notification dans Slack des modifications des tableaux et des cartes</h2>
                <p> Vous pouvez automatiquement  être averti dans Slack des modifications apportées aux tableaux et aux cartes Trello. Pour ce faire, activez le Slack Power-Up sur le tableau qui vous intéresse, puis cliquez sur le bouton Slack qui devrait maintenant se trouver en haut à droite du tableau. De là, vous pourrez configurer les modifications desquelles vous souhaitez être alerté et de l'endroit dans Slack.</p>
                <p><img src="../img/slack_power" style="width: 845px;"></p>
                <p>Le bouton Slack fera remonter une boîte de dialogue vous permettant de visualiser et de gérer les alertes Slack qui ont été configurées pour ce tableau.</p>
                <p><img src="../img/slack_power2" style="width: 838px;"></p>
                <p>Lorsque vous cliquez sur « Ajouter une alerte Slack », si vous n'avez pas encore donné l'autorisation avec Slack, un bouton vous sera présenté pour le faire.</p>
                <p> Ne vous inquiétez pas, vous pouvez également autoriser avec d'autres équipes Slack.</p>
                <p><img src="../img/slack_power3" style="width: 838px;"></p>
                <p>Après avoir autorisé au moins une équipe Slack, vous serez dirigé vers la nouvelle page de configuration des alertes. Vous pourrez y sélectionner l'équipe Slack à laquelle vous souhaitez que les alertes soient adressées, et la chaîne (nous soutenons les chaînes, les chaînes privées, ainsi que les messages directs).</p>
                <p><img src="../img/slack_power_up" style="width: 523px"></p>
                <p> Cliquez sur « Terminé » pour terminer et configurer l'alerte.</p>
                <p><img src="../img/slack_power4" style="width: 830px;"></p>
                <p>Excellent travail ! Vous pouvez toujours revenir pour mettre à jour ou supprimer cette alerte à tout moment. Vous pouvez ajouter autant d'alertes que vous le souhaitez à un tableau.</p>
                <h2>/trello commands</h2>
                <p> Bonjour ! Voici les commandes que vous pouvez utiliser avec Trello. Si vous avez besoin d'une aide supplémentaire, vous pouvez toujours nous joindre avec /trello feedback. </p>
                <p><strong>Ajouter une carte</strong><code>/trello add [teammates] [card name]</code> Exemple : <code>/trello add @john @jessica Finish blog post</code></p>
                <p><strong>Recherche</strong><code>/trello search [terms]</code> Exemple : <code>/trello search meeting agenda</code></p>
                <p><strong>Associer une chaîne à un tableau</strong><code>/trello link [search term or board url]</code> Exemple : <code>/trello link Project Awesome</code> ou <code>/trello link https://trello.com/b/nCQJJoZ/project-awesome</code></p>
                <p><strong>Changer la liste à laquelle les cartes sont ajoutées</strong><code>/trello set-list</code></p>
                <p><strong>Afficher les détails d'une carte ou d'un tableau</strong><code>/trello [url]</code> Exemple : <code>/trello https://trello.com/b/nC8JJoZ/project-awesome</code></p>
                <p><strong>Nous envoyer un commentaire</strong><code>/trello feedback I love tacos!</code></p>
                <p><strong>Voir les informations du tableau associé d'une chaîne</strong><code>/trello info</code></p>
                <p><strong>Dissocier les équipes Trello et Slack</strong><code>/trello unlink-team</code> Remarque : vous devez être administrateur de l'équipe Slack ou de l'équipe Trello associée pour utiliser cette commande. Toute votre équipe ne pourra plus utiliser Trello dans Slack. Vous ne pourrez plus l'utiliser <code>/trello</code>, les boutons ou voir les détails des éléments de Trello à l'intérieur de Slack. </p>
                <p><strong>Dissocier une chaîne d'un tableau Trello</strong><code>/trello unlink</code></p>
                <p><strong>Supprimez et retirez l'autorisation du compte utilisateur Trello associé</strong><code>/trello reset</code></p>
                <p><strong>Vérifiez le compte utilisateur Trello associé et l'appartenance à l'équipe</strong><code>/trello setup</code></p>
                <p><strong>Remarque</strong>: les commandes ci-dessous mettront à jour la dernière carte affichée par @trello ou ajoutée via @trello <code>/trello add</code></p>
                <p><strong>Affecter des coéquipiers à une carte</strong><code>/trello assign @john @jessica</code></p>
                <p><strong>Ajouter un commentaire à une carte</strong><code>/trello comment [comment text]</code> Exemple : <code>/trello comment That’s awesome!</code></p>
                <p><strong>Ajouter une date d'échéance</strong><code>/trello set-due next Friday at 5pm</code> ou <code>/trello set-due today</code></p>
                <p><strong></strong><code></code><code></code></p>
                <p><strong></strong><code></code><code></code></p>
            </div>
        </section>
        <?php include __DIR__ . "/../php/footer.php" ?>
        </footer>
    </body>
</html>