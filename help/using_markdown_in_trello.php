<link rel="stylesheet" href="/yep_project1_2019/css/help.css" type="text/css"/>

<html>
    <?php include __DIR__ . "/../php/head.php" ?>
    </head>
    <body>
        <?php include __DIR__ . "/../php/header.php" ?>
        <section class="main-content">
            <div class="contentWrapper">
                <h1>Comment formater votre texte sur Trello</h1>
                <p> Markdown est un langage simple utilisé pour ajouter du formatage au texte. Trello utilise une version modifiée de la syntaxe Markdown. Il vous permet d'ajouter facilement des caractères gras, italiques, des liens, des listes, des paragraphes, des en-têtes, des images et des blocs de code à des blocs de texte. Vous pouvez ajouter une réduction à vos cartes dans la description de la carte, des listes de contrôle et des commentaires, ainsi que dans votre bio Trello.</p>
                <h2>Syntaxe Markdown</h2>
                <p> Markdown ne fonctionne pas pour les titres de carte, et toute la syntaxe ne s'affichera pas sur l'application mobile Trello.</p>
                <h4>Syntaxe pour les descriptions de cartes, les commentaires, les objets de la checklist et votre bio :</h4>
                <p><strong>Texte en gras</strong> - Encadrez votre texte de doubles astérisques, comme **ceci**, pour le mettre en gras.</p>
                <p><strong>Texte en italique</strong> - Encadrez votre texte avec de simples astérisques , comme *ceci*, pour le mettre en italique. Vous pouvez également utiliser les caractères de soulignement pour obtenir le même effet, par exemple : _ceci_.</p>
                <p><strong>Texte biffé</strong> - Rayez votre texte en l'encadrant de deux tildes de chaque côté, comme ~~ceci~~.</p>
                <p><strong>Code inline</strong> - Intégrez du code inline formaté en l'encadrant d'un simple backtick (`) au début et à la fin du code.</p>
                <p><strong>Liens </strong>- Créez un lien en plaçant le texte à lier entre crochets et l'adresse URL entre parenthèses, comme [ceci](http://www.trello.com).</p>
                <p><img src="../img/markdown1" style="width: 250px; float: left; margin: 0 10 10 0;" alt></p>
                <p><img src="../img/markdown2" style="width: 250px" alt></p>
                <p> Vous pouvez copier et coller le texte suivant pour avoir un modèle sur vos tableaux:</p>
                <p> Ce texte est ** gras **</p>
                <p> Ce texte est _italic_</p>
                <p> Ce texte est ~~ dépassé ~~</p>
                <p> Ce texte est `code`</p>
                <p> Il s'agit d'un [lien] (http://www.trello.com)</p>
                <h4>Syntaxe pour les descriptions de cartes et les commentaires uniquement :</h4>
                <p><strong>Ligne horizontale</strong> - Une ligne d'au moins trois traits d'union créera une ligne horizontale sur toute la largeur de la description ou du commentaire. Dans la description de la carte, placez un saut de ligne supplémentaire entre votre texte et la ligne de traits d'union pour empêcher ces derniers d'être interprétés comme un en-tête par la syntaxe.</p>
                <p><strong>Bloc de code </strong>- Intégrez du code formaté en l'encadrant de trois backticks (```) au début et à la fin du bloc, ou en démarrant une ligne par quatre espaces. Notez que les triples backticks doivent se trouver sur une ligne distincte et assurez-vous d'avoir une ligne vierge avant et après le bloc de code.</p>
                <p><strong>Faire une alinéa / Citation de bloc </strong> - Faites un alinéa avant votre texte en intégrant « > » devant chaque ligne de texte que vous désirez décaler ou citer.</p>
                <p><strong></strong></p>
                <p><strong></strong></p>
                <p><strong>Listes à puces et numérotées:</strong> Préface une série de lignes avec des tirets ou des nombres pour créer une liste. Les listes ne seront formatées que si vous commencez un nouveau paragraphe (en laissant une ligne vide avant la liste). Vous pouvez ajouter un espace avant la marque de la puce pour créer des puces imbriquées. Pour mettre en retrait dans une liste à puces ou numérotée, commencez la nouvelle ligne par un espace.</p>
                <p> Chaque ligne démarrant avec un format numéraire, même si le nombre en question n'est pas 1, créera automatiquement une liste ordonnée. Ceci est prévu et découle des caractéristiques de Markdown. Vous pouvez préserver votre formatage en plaçant un '\' avant le point final, par exemple <code>28\. March</code>.</p>
                <h3>Syntaxe ne fonctionnant que dans la description des cartes :</h3>
                <p><strong>Images intégrées</strong> - Intégrez une image en plaçant le texte du lien entre crochets et l'adresse URL ou le chemin vers l'image entre parenthèses, précédées d'un point d'exclamation, comme : ![Texte alt](/chemin/vers/img.jpg)</p>
                <p><strong>Échapper à Markdown</strong> - Pour utiliser littéralement la syntaxe de markdown, vous pouvez échapper à la mise en forme en utilisant une barre oblique inversée '\' devant les symboles, par exemple <code>\*literal asterisks\*</code></p>
                <p> Il s'agit de toute la syntaxe Markdown disponible sur Trello, mais si vous désirez en savoir plus sur Markdown en général, vous pouvez voir la syntaxe complète sur Daring Fireball</p>
            </div>
        </section>
        <?php include "../php/footer.php" ?>
        </footer>
    </body>
</html>