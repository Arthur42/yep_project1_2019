<link rel="stylesheet" href="/yep_project1_2019/css/help.css" type="text/css"/>

<html>
    <?php include __DIR__ . "/../php/head.php" ?>
    </head>
    <body>
        <?php include __DIR__ . "/../php/header.php" ?>
        <section class="main-content">
            <div class="contentWrapper">
                <h1>Trello Or Guide de l'utilisateur</h1>
                <p> Voulez-vous obtenir Trello or pour votre compte? Mise à niveau à trello.com/gold</p>
                <h2>Autocollants</h2>
                <p> Pour utiliser les autocollants supplémentaires, allez dans le menu sur le côté droit de votre tableau et sélectionnez "Autocollants". Vous pouvez faire glisser et déposer l'un des autocollants disponibles et télécharger vos propres autocollants en cliquant sur le bouton + dans la section «Autocollants personnalisés».</p>
                <h2>milieux du tableau</h2>
                <p> Pour télécharger votre propre arrière-plan personnalisé, allez dans le menu sur le côté droit de votre tableau et sélectionnez "Changer l'arrière-plan". Cliquez sur le bouton + dans la section «Personnalisé» pour télécharger votre propre image.</p>
                <h2>Grandes pièces jointes</h2>
                <p> Vous pouvez télécharger des fichiers jusqu'à 250 Mo. Pour attacher des fichiers aux cartes, sélectionnez un fichier via "Attacher un fichier ..." à l'arrière de la carte ou faites glisser et déposez depuis votre bureau. Les 250 Mo sont pour les fichiers individuels, pas une limite totale, vous pouvez donc avoir plusieurs fichiers volumineux sur une seule carte ou carte.</p>
                <h2>Fonctionnalités et quotas supplémentaires de majordome</h2>
                <p>Gold déverrouille le quota personnel de Butler avec 200 commandes exécutées par mois regroupées sur toutes les cartes, et les onglets Calendrier et Date d'échéance deviennent disponibles.</p>
                <h2>Uploading personnalisé emoji</h2>
                <p> Pour télécharger emoji, ouvrir le champ de commentaire sur une carte, cliquez sur le bouton de menu dans le coin inférieur droit, et sélectionnez "Ajouter Emoji". Vous serez alors en mesure de télécharger et nommez votre emoji personnalisé.</p>
                <h2>recherche sauvegardée</h2>
                <p> Pour enregistrer une recherche, entrez vos termes de recherche et les opérateurs, cliquez sur "Enregistrer cette recherche", le nom de votre recherche, puis cliquez sur "Enregistrer". Cliquez sur "Retour à Recherches sauvegardées" pour afficher vos recherches enregistrées. Pour exécuter une recherche enregistrée, cliquez simplement dessus.</p>
                <h2>La page de facturation</h2>
                <p> Vous pouvez voir quand votre compte sera renouvelé, mettez à jour vos informations de carte de crédit, consultez l'historique de facturation et ainsi de suite dans la section "Facturation" de votre profil d'utilisateur. Il suffit d' aller à https://trello.com/your/billing pour y accéder. Si vous vous êtes inscrit via des crédits, vous pouvez vérifier si vos crédits expirent et vous pouvez entrer une carte de crédit et vous inscrire pour un abonnement. Vous pouvez annuler Trello or via un lien en bas de la page de facturation.</p>
            </div>
        </section>
        <?php include __DIR__ . "/../php/footer.php" ?>
        </footer>
    </body>
</html>