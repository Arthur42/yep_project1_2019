<link rel="stylesheet" href="/yep_project1_2019/css/help.css" type="text/css"/>

<html>
    <?php include __DIR__ . "/../php/head.php" ?>
    </head>
    <body>
        <?php include __DIR__ . "/../php/header.php" ?>
        <section class="main-content">
            <div class="contentWrapper">
                <h1>Effacer un tableau</h1>
                <p> Pour effacer un tableau, vous devrez d'abord le fermer. Fermer un tableau se fait de la même manière que l'archivage d'une carte : vous pouvez le laisser dans votre liste « Tableaux Fermés » si vous pensez en avoir un jour à nouveau besoin, ou vous pouvez l'effacer définitivement une fois qu'il sera fermé.</p>
                <p> La suppression d'une carte est permanente et les cartes supprimées ne peuvent pas être récupérées.</p>
                <h2>Fermer un tableau</h2>
                <p> Fermez une planche en sélectionnant "Fermer la carte" dans le menu du tableau (ouvrez le menu, cliquez sur le bouton "Plus" pour agrandir les options du menu). Vous devez être administrateur d'un tableau pour le fermer. Dans les équipes de Business Class, les administrateurs de l'équipe peuvent également fermer les tableaux.</p>
                <p><img src="../img/close_board" style="width: 403px; display: block; margin: auto;" alt></p>
                <p style="text-align: center;">Fermer une tableau à partir du menu de la tableau.</p>
                <p> Lorsqu'un tableau est fermé, il n'est plus visible depuis votre liste de tableaux et ne peut être accédé qu'en cliquant sur le bouton Tableaux en haut à gauche de votre page Trello, puis en sélectionnant « Voir les tableaux fermés... » </p>
                <p><img src="../img/see_close_board" style="width: 224.222px; display: block; margin: auto;" alt></p>
                <p style="text-align: center;"> Accès fermé tableaux de votre tiroir tableaux. </p>
                <h2>Effacer un tableau</h2>
                <p> Pour effacer un tableau que vous avez déjà fermé, ouvrez d'abord votre liste de Tableaux Fermés. Vous y trouverez une liste de vos tableaux fermés, avec un bouton « Effacer » à côté du bouton « Rouvrir » pour tout tableau dont vous êtes administrateur. Vous pouvez cliquer sur ce bouton pour effacer le tableau définitivement.</p>
                <p><img src="../img/reopen_board" style="width: 224.222px; display: block; margin: auto;" alt></p>
                <h2>Récupérer un tableau supprimé</h2>
                <p> La suppression d'une carte est permanente et les cartes supprimées ne peuvent pas être récupérées.</p>
            </div>
        </section>
        <?php include __DIR__ . "/../php/footer.php" ?>
        </footer>
    </body>
</html>