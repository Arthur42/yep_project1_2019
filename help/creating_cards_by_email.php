<link rel="stylesheet" href="/yep_project1_2019/css/help.css" type="text/css"/>

<html>
    <?php include __DIR__ . "/../php/head.php" ?>
    </head>
    <body>
        <?php include __DIR__ . "/../php/header.php" ?>
        <section class="main-content">
            <div class="contentWrapper">
                <h1>Création de cartes par email</h1>
                <p>Vous pouvez créer des cartes sur une tableau en écrivant ou en transmettant un courriel à Trello.</p>
                <p> Pour obtenir votre adresse e-mail pour un forum, connectez-vous à Trello dans le navigateur et ouvrez le forum sur lequel vous souhaitez créer des cartes par email. Ouvrez le menu dans la barre latérale de droite et sélectionnez "Plus", puis "Paramètres Email-to-Board". À partir de là, vous pouvez sélectionner l'endroit où la nouvelle carte sera créée sur le plateau. Vous pouvez cliquer sur le nom de la liste en cours ('Aucun' dans l'exemple ci-dessous) pour changer cela en une liste différente. </p>
                <p> Vous pouvez aussi avoir votre adresse e-mail pour que le tableau par courriel. S'il vous plaît noter que l'adresse e-mail est unique pour chaque membre du tableau et du tableau. En outre, les commentaires doivent être activés pour le tableau afin de générer l'adresse e-mail à tableau.</p>
                <p> La carte sera affiché dans les prochaines minutes après l'e-mail est envoyé. </p>
                <br>
                <p><img src="../img/add_card_via_mail" style="display: block; margin: auto; width: 373px;"></p>
                <p> Chaque tableau Trello a une adresse e-mail unique. </p>
                <h2>Conseils de mise en forme</h2>
                <p> Lorsque vous envoyez une carte à Trello, vous pouvez immédiatement ajouter des pièces jointes, les étiquettes et les membres à la carte, en plus de définir le titre et la description.</p>
                <ul>
                    <li>
                        <p>Le sujet de l'email devient le titre de la carte.</p>
                    </li>
                    <li>
                        <p>Le corps de l'email devient la description de la carte.</p>
                    </li>
                    <li>
                        <p>Pièces jointes dans l'e-mail sera ajouté à la carte.</p>
                    </li>
                    <li>
                        <p>Libellés: Dans le sujet, ajoutez #labelname, #labelcolor ou #labelnumber </p>
                        <ul>
                            <li>
                                <p>Si votre étiquette se compose de deux mots ou plus, dans la ligne d'objet, soit rejoindre les mots ou l'utilisation souligne entre les mots. Par exemple, si votre étiquette est nommé "To Do" dans la ligne objet de votre email soit entrer #ToDo ou #To_Do pour l'étiquette de rendre correctement sur votre carte.</p>
                            </li>
                            <li>
                                <p>Si vous avez plusieurs étiquettes dans un tableau avec la même couleur ou le même nom, courriel une tableau avec #color ou #name ajoutera seulement la première étiquette avec cette couleur ou le nom de la carte.</p>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <p>Membres: Dans le sujet, ajouter @username. Les membres peuvent également être ajoutés en mettant @username dans le corps de l'e-mail sur sa propre ligne. Si vous envoyez un courriel à Trello et inclure les adresses e-mail des autres utilisateurs Trello que "à" ou adresses "CC", Trello les ajoutera en tant que membres de la carte ainsi.</p>
                    </li>
                </ul>
                <h2>Accessoires et Email Size</h2>
                <p> La plupart des e-mails avec pièces jointes en faire Trello très bien. Cependant, il y a une limite de taille de messagerie globale (ci-dessus 10MB) qui empêche les cartes d'être créé par courriel. Si cela se produit, votre serveur de messagerie peut vous envoyer un message qu'il n'a pas pu envoyer le courriel.</p>
                <h2>Email Threads and Duplicated Cards</h2>
                <p>The first time a message is sent to the board email address, it will create a new card. If that same address receives more messages from the same sender address and with the same subject, the messages will be added to the original card as comments. This could happen when the board email address is added to a long email thread, and it is designed this way to prevent duplicated cards.</p>
                <h2>Email Commentaires sur carte </h2>
                <p> Chaque carte a également son propre email - envoyer une note à cette adresse fera apparaître votre email sous forme de commentaire. Vous pouvez trouver le courrier électronique d'une carte sous le bouton "Partager" sur le dos de la carte, dans le coin inférieur droit. </p>
                <p><img src="../img/share_card" style="display: block; margin: auto; width: 373px;"></p>
                <h2>Créer des cartes par email depuis nos applications mobiles</h2>
                <p> S'il vous plaît noter que ce n'est actuellement pas disponible. Vous devrez vous connecter à Trello sur un navigateur Web afin d'obtenir ces adresses e-mail.</p>
            </div>
        </section>
        <?php include __DIR__ . "/../php/footer.php" ?>
        </footer>
    </body>
</html>