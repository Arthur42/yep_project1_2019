<link rel="stylesheet" href="/yep_project1_2019/css/help.css" type="text/css"/>

<html>
    <?php include __DIR__ . "/../php/head.php" ?>
    </head>
    <body>
        <?php include __DIR__ . "/../php/header.php" ?>
        <section class="main-content">
            <div class="contentWrapper">
                <h1>Utilisez Trello avec le Google Agenda</h1>
                <p> Si vous utilisez Google Agenda, vous pouvez y intégrer tous vos calendriers de tableaux Trello, afin de pouvoir voir vos calendriers et vos cartes avec leurs délais rassemblés sur une page.</p>
                <p> Pour commencer, activez le fil iCalendar et trouvez l'adresse URL iCalendar pour votre tableau.</p>
                <ol>
                    <li>
                        <p>Ouvrez le menu tableau</p>
                    </li>
                    <li>
                        <p>Sous "Power-Ups", cliquez sur "Calendrier". Activez le Power-Up si vous ne l'avez pas déjà fait en cliquant sur "Ajouter Power-Up".</p>
                    </li>
                    <li>
                        <p>Une fois là-bas, cliquez sur Modifier les paramètres du Power-Up. Vous pouvez également ouvrir le Power-Up Calendrier depuis le tableau en cliquant sur l'icône d'engrenage.</p>
                    </li>
                    <li>
                        <p>Copiez l'URL du "Flux iCalendar".</p>
                    </li>
                </ol>
                <p><img src="../img/calendar_settings" alt style="display: block; margin: auto;"></p>
                <p> Ensuite, ouvrez Google Agenda et cliquez sur l'icône des points de suspension en regard de "Ajouter un agenda" sur le côté gauche Cliquez sur "De l'URL".</p>
                <p><img src="../img/add_other_calendars" style="display: block; margin: auto; width: 100%; max-width: 350px;"></p>
                <p><img src="../img/add_calendar_from_url" style="display: block; margin: auto; width: 100%; max-width: 350px;"></p>
                <p> Coller l'adresse URL iCalendar dans le champ de saisie, et cliquer sur « Ajouter un calendrier. »</p>
                <p> Google Agenda ajoutera votre tableau de calendrier Trello et une couleur lui sera attribuée. Vous trouverez le calendrier listé dans la section « Autres Calendriers ».</p>
            </div>
        </section>
        <?php include __DIR__ . "/../php/footer.php" ?>
        </footer>
    </body>
</html>