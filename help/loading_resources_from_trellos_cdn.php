<link rel="stylesheet" href="/yep_project1_2019/css/help.css" type="text/css"/>

<html>
    <?php include __DIR__ . "/../php/head.php" ?>
    </head>
    <body>
        <?php include __DIR__ . "/../php/header.php" ?>
        <section class="main-content">
            <div class="contentWrapper">
                <h1>Chargement des ressources du CDN de Trello</h1>
                <p> Trello utilise un service tiers appelé Réseau de diffusion de contenu (CDN) pour rendre Trello plus rapide. Un CDN est utilisé pour vous fournir des fichiers qui sont plus proches de votre situation géographique.</p>
                <p> Ces ressources ne provenant pas de trello.com, elles sont parfois bloquées par des pare-feu d'entreprise. Si cela se produit, Trello ne fonctionnera pas.</p>
                <p> Si vous recevez un message dans Trello indiquant «Votre navigateur n'a pas pu charger toutes les ressources de Trello», contactez votre administrateur réseau et demandez-leur d'autoriser l'accès à <strong>d78fikflryjgj.cloudfront.net,</strong><strong>d2k1ftgv7pobq7.cloudfront.net</strong> et <strong>a.trellocdn.com</strong>.</p>
                <p> Les causes les plus courantes du non-chargement de <strong>d78fikflryjgj.cloudfront.net</strong> et <strong>a.trellocdn.com</strong> sont :</p>
                <ul>
                    <li>
                        <p>Le DNS n'est pas résolu</p>
                    </li>
                    <li>
                        <p>Un pare-feu bloque le <strong>d78fikflryjgj.cloudfront.net, d2k1ftgv7pobq7.cloudfront.net</strong> ou <strong>a.trellocdn.com</strong></p>
                    </li>
                    <li>
                        <p>Un périphérique netnanny bloque le <strong>d78fikflryjgj.cloudfront.net</strong> , <strong>d2k1ftgv7pobq7.cloudfront.net</strong> ou <strong>a.trellocdn.com</strong></p>
                    </li>
                    <li>
                        <p>Problèmes de navigateur : essayez de supprimer le cache de votre navigateur pour voir si cela est utile</p>
                    </li>
                    <li>
                        <p>Supprimez le cache DNS de votre machine locale. Voir les Instructions Windows ou les Instructions Mac</p>
                    </li>
                </ul>
            </div>
        </section>
        <?php include __DIR__ . "/../php/footer.php" ?>
        </footer>
    </body>
</html>