<link rel="stylesheet" href="/yep_project1_2019/css/help.css" type="text/css"/>

<html>
    <?php include __DIR__ . "/../php/head.php" ?>
    </head>
    <body>
        <?php include __DIR__ . "/../php/header.php" ?>
        <section class="main-content">
            <div class="contentWrapper">
                <h1>Archivage et suppression de cartes</h1>
                <h2>cartes d'archivage</h2>
                <p> cartes d'archives en cliquant sur "Carte Archive" dans le menu Actions sur le dos d'une carte. Ou vous pouvez également sélectionner "Archive" dans le menu de la carte (cliquez sur l'icône de crayon qui apparaît sur le vol stationnaire). Vous pouvez également archiver avec le "c" raccourci clavier lorsque le curseur de votre souris sur une carte.</p>
                <p><img src="../img/card_actions" style="width: 150px; margin: auto;">
                <img src="../img/card_actions2" style="margin: auto; width: 350px;"></p>
                <p style="text-align: center;">cartes Archive vous n'êtes plus besoin sur votre tableau.</p>
                <h4>FAQ: Les cartes archivées seront-elles supprimées éventuellement?</h4>
                <p> Les cartes archivées resteront archivées indéfiniment. Bien que vous ou un autre membre du conseil d'administration puisse choisir de supprimer ces cartes ou de les renvoyer à la carte à tout moment, elles ne seront pas supprimées automatiquement. Vous pouvez les laisser archiver pendant aussi longtemps que vous le souhaitez et les récupérer ultérieurement chaque fois que vous en avez besoin. </p>
                <h2>cartes désarchivage</h2>
                <p> Dans le menu de la tableau, cliquez sur "Plus" "éléments archivés" puis, trouver la carte et cliquez sur "Envoyer à bord." </p>
                <p><img src="../img/archived_items" style="width: 317px; display: block; margin: auto;"></p>
                <p><img src="../img/send_to_board" style="display: block; margin: auto; width: 318px;"></p>
                <p> La barre de recherche fera apparaître une liste des cartes archivées qui correspondent à vos termes. Vous pouvez utiliser les mêmes opérateurs spéciaux comme la recherche bar-voir pour une liste complète. </p>
                <h2>cartes Suppression</h2>
                <p> Pour supprimer une carte, vous devez d'abord l'archiver. Cliquez sur la carte pour l'ouvrir, puis choisissez "Archive" en bas à droite. Après l’archivage, une nouvelle option "Supprimer" sera disponible. La suppression d’une carte étant irrécupérable et permanente, un clic de confirmation est nécessaire. </p>
                <p><img src="../img/card_suppression" style="display: block; margin: auto;"></p>
                <p style="text-align: center;">Suppression de cartes est permanent. </p>
                <p> La suppression d'une carte est permanente et les cartes supprimées ne peuvent pas être restaurées. Si vous souhaitez simplement effacer la carte de votre carte, vous pouvez l'archiver à la place, afin que vous puissiez y accéder plus tard, si nécessaire. </p>
            </div>
        </section>
        <?php include __DIR__ . "/../php/footer.php" ?>
        </footer>
    </body>
</html>