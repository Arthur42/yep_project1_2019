<link rel="stylesheet" href="css/application.css" type="text/css" />

<html>
    <?php include "php/head.php" ?>
    </head>
    <body>
        <?php include "php/header.php" ?>

        <div class='section-platforms-app'>
        	<h1><?php echo _applicationTitle; ?></h1>
        
	        <div class="web-app">
	        	<img src="img/web-icon.png">
	        	<h2><?php echo _web; ?></h2>
	        </div>
	        <div class="mobile-app">
	        	<img src="img/mobile-icon.png">
	        	<h2><?php echo _mobileDevice; ?></h2>
	        </div>
	        <div class="desktop-app">
	        	<img src="img/desktop-icon.png">
	        	<h2><?php echo _desktop; ?></h2>
	        </div>
	    </div>
	    <div class='section-platforms-browsers'>
        	<h1><?php echo _applicationTitle2; ?></h1>
        
	        <div class="browser-icon">
	        	<img src="img/icon-chrome.png">
	        	<h3>Chrome</h3>
	        </div>
	        <div class="browser-icon">
	        	<img src="img/icon-firefox.png">
	        	<h3>Firefox</h3>
	        </div>
	        <div class="browser-icon">
	        	<img src="img/icon-edge.png">
	        	<h3>Edge</h3>
	        </div>
	    </div>
	    <?php include "php/footer.php" ?>
    </body>
</html>