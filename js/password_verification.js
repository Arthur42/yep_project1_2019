$(document).ready(function(){
    $('#form').on('submit', function(e){
        var p = $('#password').val();
        var c = $('#password_check').val();

        if (p != c) {
            e.preventDefault();
            $('#form').after('<p style=\'color: red\'> Les mots de passes ne correspondent pas entre eux</p>');
        }
    });
});