$(document).ready(function(){
    $(".popup-button").click(function(event) {
        $(".popup").addClass("popup-hidden");
        var popup = $(event.currentTarget).siblings(".popup-hidden");
        popup.removeClass("popup-hidden");
        event.stopPropagation();
    });
    
    $(".popup-close").click(function(event) {
        $(".popup").addClass("popup-hidden");
        event.stopPropagation();
    });
    
    $(".popup").click(function(event) {
        event.stopPropagation();
    });

    
    $("body").click(function(event){
        $(".popup").addClass("popup-hidden");
    });
    
    $(".popup-create-board").click(function(event){
        $(".popup").addClass("popup-hidden");
        $(".popup-create-board-content-out").removeClass("popup-hidden");
    });
    
    $(".popup-create-board-content-in").click(function(event){
        event.stopPropagation();
    });
    
    $(".popup-create-board-content-out").click(function(event){
        $(".popup-create-board-content-out").addClass("popup-hidden");
    });
    
    $(".popup-column").click(function(event){
        console.log("test");
        var popup = $(event.currentTarget).siblings(".popup-hidden");
        popup.removeClass("popup-hidden");
    });
});


/* usage exemple : */

/*
<div class="popup-container">
    <i class="icon-info popup-button"></i>
    <div class="popup popup-hidden">
        <div class="popup-header">
            <div class="popup-title"><h1>Informations</h1></div>
                <div class="popup-close">
                <div class="popup-close-icon">
                    <i class="icon-angle-up"></i>
                </div>
            </div>
        </div>
        <div class="popup-content">
            <p>Dum apud Persas, ut supra narravimus, perfidia regis motus agitat insperatos.</p>
        </div>
        <div class="popup-content">
            <p>Quas obliterasset utinam iuge silentium! ne forte paria quandoque temptentur, plus exemplis generalibus nocitura quam delictis.</p>
        </div>
    </div>
</div>
*/