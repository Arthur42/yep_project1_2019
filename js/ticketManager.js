$(window).ready(() => {

    var o = false;
    var ci = -1;
    var cc = -1;

    $(document).on('click', '.ticket_create', function() {
        $(this).parent().attr('id', 'open');
        $(this).parent().html('<textarea placeholder=\'Titre du ticket\' id=\'create_title\'></textarea><br /><input type=\'submit\' class=\'ticket_add\' value=\'Ajouter\' id=\'create\'/>');
        setTimeout(function(){o=true}, 500);
    });

    function close_creator() {
        $('#open').html('<a href=\'#\' class=\'ticket_create\'>Ajouter un ticket</a>');
        $('#open').attr('id', '');
        o = false;
    }

    $(document).click(function(e) {
        if(!$(e.target).is('#open') && !$(e.target).is('#create_title') && !$(e.target).is('#create') && o === true) {
            close_creator();
        }
    });

    function register_ticket(t, i) {
        $.ajax({
            type: "POST",
            context: this,
            url: "/yep_project1_2019/ticket/create",
            data: {
                title: t,
                c: i
            },
            success: function (r) {
                if (r.status == 'ok') {
                    $('#open').before('<a href=\'' + r.idcolumn + '/' + r.id + '\' class=\'ticket\'>' + t + '</a>');
                    close_creator();
                } else {
                    alert(r.status);
                }
            },
            error: function(response, status, error) {
                alert(error);
            }
        });
    }

    $(document).on('keypress', '#create_title', function (e) {
        let t = $('#create_title').val();
        let c = $(this).parent().parent().attr('id');
        let i = c.replace('column-', '');
        if (e.which == 13) {
            if (t == '') {
                e.preventDefault();
                return false;
            }
            register_ticket(t, i);
        }
    });

    $(document).on('click', '#create', function(){
        let t = $('#create_title').val();
        let c = $(this).parent().parent().attr('id');
        let i = c.replace('column-', '');
        if (t == '')
            return;
        register_ticket(t, i);
    });

    $(document).on('click', '.ticket', function(e){
        let title = $(this).html();
        let s = $(this).attr('href').split('/'), c = s[0], i = s[1];
        $.ajax({
            type: 'POST',
            context: this,
            url: '/yep_project1_2019/ticket/modifier',
            data: {
                id: i,
                idcolumn: c
            },
            success: function(r) {
                if (r.status == 'ok') {
                    ci = i;
                    cc = c;
                    $('body').append(r.content);
                } else {
                    alert('Error: ' + r.status);
                }
            },
            error: function(r, s, er) {
                alert(r.status);
                alert(er);
            }
        });
        e.preventDefault();
        return false;
    });

    $(document).on('click', '.ticket_background', function(){
        $('.ticket_background').remove();
        $('.modifier').remove();
    });

    function modify_ticket_title(nt) {
        $.ajax({
            type: 'POST',
            context: this,
            url: '/yep_project1_2019/ticket/modify_title',
            data: {
                title: nt,
                id: ci,
                idcolumn: cc
            },
            success: function(response) {
                if (!response.status === 'ok')
                    alert(response.status);
            },
            error: function(response, status, error) {
                alert(error);
            }
        })
    }

    $(document).on('focusout', '#modify_title', function(){
        modify_ticket_title($(this).val());
    });

    $(document).on('keypress', '#modify_title', function (e) {
        let t = $('#modify_title').val();
        if (e.which == 13) {
            if (t == '') {
                e.preventDefault();
                return false;
            }
            modify_ticket_title(t);
        }
    });

    function modify_ticket_description(d) {
        $.ajax({
            type: 'POST',
            context: this,
            url: '/yep_project1_2019/ticket/modify_description',
            data: {
                description: d,
                id: ci,
                idcolumn: cc
            },
            success: function(response) {
                if (!response.status === 'ok')
                    alert(response.status);
            },
            error: function(response, status, error) {
                alert(error);
            }
        })
    }

    $(document).on('focusout', '#modify_description', function(){
        modify_ticket_description($(this).val());
    });

    $(document).on('keypress', '#modify_description', function (e) {
        let t = $('#modify_description').val();
        if (e.which == 13) {
            if (t == '') {
                e.preventDefault();
                return false;
            }
            modify_ticket_description(t);
        }
    });

    $(document).on('click', '.delete_button', function(e) {
        let s = confirm('Etes-vous sûr de vouloir supprimer le ticket ?');

        if (s) {
            $.ajax({
                type: 'POST',
                context: this,
                url: '/yep_project1_2019/ticket/delete',
                data: {
                    id: ci,
                    idcolumn: cc
                },
                success: function(response) {
                    if (response.status != 'ok')
                        alert(response.status);
                    else {
                        $('.ticket_background').remove();
                        $('.modifier').remove();
                        $('a[href="' + cc + '/' + ci + '"]').remove();
                    }
                },
                error: function(response, status, error) {
                    alert(error);
                }
            });
        }
    });

    function register_comment(c) {
        $.ajax({
            type: 'POST',
            context: this,
            url: '/yep_project1_2019/ticket/add_comment',
            data: {
                idticket: ci,
                idcolumn: cc,
                comment: c
            },
            success: function (r) {
                if (r.status == 'ok') {
                    $('#comments').after(r.comment);
                    $('#comment_text').val('');
                } else
                    alert(r.status);
            },
            error: function (r, s, er) {
                alert(er);
                alert(r.status);
            }
        })
    }

    $(document).on('click', '.add_comment', function() {
        let c = $('#comment_text').val();
        if (c != '')
            register_comment(c);
    });

    $(document).on('keypress', '#comment_text', function(e) {
        let c = $('#comment_text').val();
        if (e.which == 13) {
            if (t == '') {
                e.preventDefault();
                return false;
            }
            register_comment(c);
        }
    });
    
    $(document).on('click', '.delete_comment', function(e) {
        let c = $(this).attr('href');
        $.ajax({
            type: 'POST',
            context: this,
            url: '/yep_project1_2019/ticket/delete_comment',
            data: {t: ci, c: cc, i: c},
            success: function(r) {
                if (r.status == 'ok')
                    $(this).parent().remove();
                else
                    alert(r.status);
            },
            error: function(r, s, er) {alert(er);}
        });
        e.preventDefault();
        return false;
    });
});
