/************ search bar ************/

const cardList2 = [
  {
    name: "sort items",
    link: 'pr1'
  },
  {
    name: 'do menu',
    link: 'pr2'
  }
]

const fuseTabOptions = {
    includeScore: false,
    keys: ['name'],
    includeScore: true
}
const fuseCardOptions = {
    includeScore: false,
    keys: ['name'],
    includeScore: true
}
const fusePersonOptions = {
    includeScore: false,
    keys: ['name']
}


const fuseTabList = new Fuse(boardList, fuseTabOptions)
const fuseCardList = new Fuse(cardList, fuseCardOptions)
const fusePersonList = new Fuse(personList, fusePersonOptions)


$(document).ready(function(){
    
    setDefaultSearchContent();
    
    function setDefaultSearchContent() {
        var search = $('#header-search-bar').val();

        if (search && search.localeCompare("@") != 0 && search.localeCompare("#") != 0) {
            setSearchContent("Search", "No occurence found.");
        } else {
            setSearchContent("Search", "@someone<br>#card");
        }
    }
    
    function setSearchContent(title, content) {
        $('#header-search-title').html(title);
        $('#header-search-content').html(content);
    }
    
    $('#header-search-bar').on('input', function() {
        var search = $('#header-search-bar').val();
        var result = "";
        var title = "";

        if (search[0] == '@') {
            result = fusePersonList.search(search.substring(1));
            title = "people :";
        } else if (search[0] == '#') {
            result = fuseCardList.search(search.substring(1));
            title = "Cards :";
        } else {
            result = fuseTabList.search(search);
            title = "Boards :";
        }
        var output = "";

        for (var i = 0; result[i] && i < 5; i++) {
            output += '<a href="' + result[i].item.link + '"><div class="header-elem-case">' + result[i].item.name + '</div></a>';
        }
        if (output != "") {
            setSearchContent(title, output);
        } else {
            setDefaultSearchContent();
        }
    });
});

/************ board search bar ************/


$(document).ready(function(){
    
    setDefaultBoardSearchContent();
    
    function setDefaultBoardSearchContent() {
        var search = $('#header-board-search-bar').val();

        if (search) {
            setBoardSearchContent("No occurence found.");
        } else {
            var output = "";
            for (var i = 0; boardList[i] && i < 9; i++) {
                output += '<a href="' + boardList[i].link + '"><div class="header-elem-case">' + boardList[i].name + '</div></a>';
            }
            setBoardSearchContent(output);
        }
    }
    
    function setBoardSearchContent(content) {
        $('#header-board-search-content').html(content);
    }
    
    $('#header-board-search-bar').on('input', function() {
        var search = $('#header-board-search-bar').val();
        var result = "";
        var output = "";

        result = fuseTabList.search(search);
        for (var i = 0; result[i] && i < 9; i++) {
            output += '<a href="' + result[i].item.link + '"><div class="header-elem-case">' + result[i].item.name + '</div></a>';
        }
        if (output != "") {
            setBoardSearchContent(output);
        } else {
            setDefaultBoardSearchContent();
        }
    });
});