<html>
    <?php include "php/head.php" ?>
        <link rel="stylesheet" href="/yep_project1_2019/css/board_content.css" type="text/css" />
        <link rel="stylesheet" href="/yep_project1_2019/css/ticket.css" type="text/css" />
        <script src='/yep_project1_2019/js/ticketManager.js'>
    </head>
    <body>
        <?php include "php/header.php"; ?>
        <?php
            if (isset($_POST["boardName"]) && isset($_SESSION['user'])) {
                if ($bdd->createBoard($_POST["boardName"], $_SESSION['user'])) {
                    echo "<p>" . _boardCreate . "</p>";
                    echo '<script> window.location.href = \'/yep_project1_2019/board?bord=' . $_POST["boardName"] . '\' </script>';
                } else {
                    echo "<p>" . _boardCreationFailed . "</p>";
                }
            } else if (isset($_GET["board"])) {
                if (isset($_POST["addColumn"])) {
                    $bdd->addColumn($_GET["board"], $_SESSION['user'], $_POST["addColumn"]);
                }
                include "php/board_header.php";
                include "php/board_content.php";
            }
        ?>
        <?php include "php/footer.php" ?>
        </footer>
    </body>
</html>