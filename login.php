<html>
    <?php include "php/head.php" ?>
    <?php
        $_error = '';
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if ($bdd->userExist($_POST['email'], $_POST['password']) === true) {
                $_SESSION['user'] = $_POST['email'];
                echo '<script> window.location.href = \'index\' </script>';
            } else {
                $_error = _loginFailed;
            }
        }
    ?>
    </head>
    <body>
        <?php include "php/header.php" ?>
        <h1><?php echo _loginEpitrello; ?></h1>
        <form action="login" method="POST">
            <input type="text" name="email" required placeholder="<?php echo _loginEmail; ?>"/>
            <input type="password" name="password" required placeholder="<?php echo _loginpassword; ?>" />
            <input type="submit" value="<?php echo _connection; ?>"/>
        </form>
        <?php
        if ($_error != '')
            echo '<br /><p style=\'color: red\'>' . $_error . '</p><br />';
        ?>
        <a href="recovery"><?php echo _changePassword; ?></a>
        <br />
        <br />
        <a href="register"><?php echo _createAccount; ?></a>
        <?php include "php/footer.php" ?>
        </footer>
    </body>
</html>