<?php

$lang = "";

if (isset($_SESSION['user'])) {
    $lang = $bdd->getUserLang($_SESSION['user']);
}

$defaultFile = __DIR__ . '/lang_en.php';
$file = __DIR__ . '/lang_' . $lang . '.php';

if (file_exists($file)) {
    include $file;
} else if (file_exists($defaultFile)) {
    include $defaultFile;
}