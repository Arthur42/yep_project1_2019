<?php

    // php/header.php

define('_boardName', 'nom du tableau');
define('_boards', 'Tableaux');
define('_search', 'Recherche');
define('_createBoard', 'Créé un tableau');
define('_notification', 'Notification');
define('_information', 'Informations');
define('_confidentiality', 'Confidentialité');
define('_developers', 'Développeurs');
define('_help', 'Aide');
define('_application', 'Application');
define('_legal', 'Légal');
define('_create', 'Créé');
define('_createBoardText', 'Un tableau est un ensemble de cartes classées dans des listes. Utilisez-le pour gérer des projets, suivre des informations ou organiser quoi que ce soit.');
define('_createTeam', 'Créé une équipe');
define('_createTeamText', 'Une équipe est un groupe de tableaux et de personnes. Utilisez-la pour garder votre entreprise, vos loisirs, votre famille ou vos amis organisés.');

    // php/footer.php

define('_cookieTextLeft', 'Nous utilisons des cookies sur ce site. En utilisant ce site Web, nous supposerons que vous consentez aux ');
define('_cookieTextLink', 'cookies utilisés');
define('_cookieTextRight', '');
define('_cookieContinue', 'Ok, continuer');

    // application.php

define('_applicationTitle', 'Dyscovery Trello fonctionne parfaitement, où que vous soyez.');
define('_web', 'Web');
define('_mobileDevice', 'Appareil mobile');
define('_desktop', 'Bureau');
define('_applicationTitle2', 'Dyscovery Trello est optimisé pour ces navigateurs modernes.');

    // board_content.php

    // board.php

define('_boardCreate', 'Tableau créé.');
define('_boardCreationFailed', 'Echec de la création !');

    // change_information.php

define('_errorWhileUpdatingInformation', 'Une erreur c\'produite lors de la mise à jour des informations');
define('_changeInformation', 'Changement d\'information');
define('_lastName', 'Nom');
define('_firstName', 'Prénom');
define('_pseudo', 'Pseudo');
define('_confirm', 'Validé');
define('_cancel', 'Annuler');

    // cloud_terms_of_service.php

    // destroy.php

define('_destroyTitle', 'Confirmation de destruction de compte');
define('_destroyText', 'Etes vous sûr de vouloir supprimer votre compte ?');

    // email_verification.php

define('_emailVerifError', 'Erreur: le code ne correspond pas à celui envoyé.');
define('_yourVerificationCodeIs', 'Votre code de vérification est');
define('_verificationCode', 'Code de vérification');
define('_emailVerification', 'Vérification de l\'email');
define('_emailVerifCode', 'Entrer dans le champs suivant le code à 5 caractères reçu par mail');
define('_code', 'Code');
define('_check', 'Vérifier');

    // error.php

define('_errorDefaultTitle', 'Une erreur s\'est produite.');
define('_errorDefaultMessage', 'Si vous pensez que cette erreur ne devrait pas se produire, veuillez contacter un administrateur.');
define('_error400Title', 'Bad request.');
define('_error401Title', 'Auth required.');
define('_error403Title', 'Access forbidden.');
define('_error404Title', 'Page not found.');
define('_error404Message', 'Cette page peut être privée. Vous pourrez peut-être la voir en vous <a href=\"login\">connectant</a>');
define('_error404MessageNotLogged', 'Cette page peut être privée. Si quelqu\'un vous a envoyé ce lien, il se peut qu\'il doive vous inviter à l\'un de ses tableaux ou équipes.');
define('_error500Title', 'Internal error');
define('_errorCheckIdentity', 'Vous n\'êtes pas <b>' . $_SESSION['user'] . '</b> ?<a href="logout">Déconnection</a>');

    // help.php

define('_helpTitle', 'Articles les plus populaires');
define('_helpMessage1', 'Comment formater votre texte sur Trello');
define('_helpMessage2', 'Effacer un tableau');
define('_helpMessage3', 'Chargement des ressources du CDN de Trello');
define('_helpMessage4', 'Utilisez Trello avec le Google Agenda');
define('_helpMessage5', 'Trello Or Guide de l\'utilisateur');
define('_helpMessage6', 'Archivage et suppression de cartes');
define('_helpMessage7', 'Utilisation du Power-Up du calendrier');
define('_helpMessage8', 'L\'application Trello pour Slack');
define('_helpMessage9', 'Comment utiliser Trello comme un pro');
define('_helpMessage10', 'Création de cartes par email');

    // image.php

define('_fileDoesNotMatch32x32', 'Votre fichier ne correspond pas au critière de dimension 150x150');
define('_errorWhileDownloadingImage', 'Une erreur est survenue durant le téléchargement de l\'image. Veuillez retenter');
define('_importImage', 'Importation d\'image');
define('_image32x32Only', 'Seul les images de tailles 150x150 ou inférieur sont autorisées');
define('_image', 'Image');
define('_profilImage', 'Image de profile');

    // index.php

    // login.php

define('_loginFailed', 'Impossible de ce connecter, certaine informations sont fausses');
define('_loginEpitrello', 'Connection au site Epitrello');
define('_loginEmail', 'Adresse mail');
define('_loginpassword', 'Mot de passe');
define('_connection', 'Connection');
define('_changePassword', 'Changement de mot de passe');
define('_createAccount', 'Créer un compte');

    // models.php

    // our_developpers.php

define('_ourDevelopers', 'Nos Développeurs');

    // privacy_policy.php

    // recovery.php

define('_recoveryGiveEmail', 'Veuillez indiquez dans le cadre ci-dessous votre adresse mail');
define('_codeToRecoverIs', 'Le code pour le changement de mot de passe est');
define('_codeToChangePassword', 'Code pour changement de mot de passe');
define('_verificationCodeSent', 'Le code à 5 charactères de vérification a été envoyé à votre adresse mail');
define('_password', 'Mot de passe');
define('_newPassword', 'Nouveau mot de passe');
define('_passwordVerification', 'Vérification du mot de passe');
define('_newPasswordVerification', 'Vérification du nouveau mot de passe');

    // register.php

define('_registerError', 'Le compte que vous souhaitez créer exist déjà');
define('_registerAccount', 'Création d\'un compte sur Epitrello');
define('_accountCreation', 'Création du compte');

    // reset.php

define('_passwordChanged', 'Votre mot de passe à été changé avec succès');
define('_login', 'Se connecter');
define('_codeError', 'Erreur de sécuriter: le code indiqué ne correspond pas au code envoyé par mail !');
define('_newTry', 'Nouvelle tentative');

    // teams.php

    // user_setting.php

define('_userSetting', 'Paramètre utilisateur');
define('_changeProfilPicture', 'Modifier la photo de profil');
define('_changeInformations', 'Modifier les informations');
define('_changeLang', 'Changer la langue pour :');
define('_deleteaccount', 'Supprimer le compte');
define('_closeAccount', 'Fermer le compte');
define('_language', 'Langue');

    // user.php

define('_sameBoards', 'Tableaux en communs');
define('_userNotFound', 'Utilisateur non trouvé.');