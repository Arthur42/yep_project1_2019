<?php

    // php/header.php

define('_boardName', 'board name');
define('_boards', 'Boards');
define('_search', 'Search');
define('_createBoard', 'Create a board');
define('_notification', 'Notification');
define('_information', 'Informations');
define('_confidentiality', 'Confidentiality');
define('_developers', 'Developers');
define('_help', 'Help');
define('_application', 'Application');
define('_legal', 'Legal');
define('_create', 'Create');
define('_createBoardText', 'A table is a set of cards classified in lists. Use it to manage projects, track information, or organize anything.');
define('_createTeam', 'Create a team');
define('_createTeamText', 'A team is a group of tables and people. Use it to keep your business, hobbies, family or friends organized.');

    // php/footer.php

define('_cookieTextLeft', 'We use cookies on this website. By using this website, we\'ll asume you consent to ');
define('_cookieTextLink', 'the cookies we set');
define('_cookieTextRight', '');
define('_cookieContinue', 'Ok, continue');

    // application.php

define('_applicationTitle', 'Dyscovery Trello works perfectly, wherever you are.');
define('_web', 'Web');
define('_mobileDevice', 'Mobile device');
define('_desktop', 'Desktop');
define('_applicationTitle2', 'Dyscovery Trello is optimized for these modern browsers.');

    // board_content.php

    // board.php

define('_boardCreate', 'board created.');
define('_boardCreationFailed', 'creation failed !');

    // change_information.php

define('_errorWhileUpdatingInformation', 'An error occurred while updating information');
define('_changeInformation', 'Update information');
define('_lastName', 'Last name');
define('_firstName', 'First name');
define('_pseudo', 'Pseudo');
define('_confirm', 'Confirm');
define('_cancel', 'Cancel');

    // cloud_terms_of_service.php

    // destroy.php

define('_destroyTitle', 'Confirmation of account destruction');
define('_destroyText', 'Are you sure you want to delete your account ?');

    // email_verification.php

define('_emailVerifError', 'Error: the code does not match the one sent.');
define('_yourVerificationCodeIs', 'Your verification code is');
define('_verificationCode', 'Verification code');
define('_emailVerification', 'Email verification');
define('_emailVerifCode', 'Enter the 5-character code received by email in the following fields');
define('_code', 'Code');
define('_check', 'Check');

    // error.php

define('_errorDefaultTitle', 'An error has occur.');
define('_errorDefaultMessage', 'if you think that this error should not occur, please contact an administrator.');
define('_error400Title', 'Bad request.');
define('_error401Title', 'Auth required.');
define('_error403Title', 'Access forbidden.');
define('_error404Title', 'Page not found.');
define('_error404Message', 'This page can be private. You may be able to view it by <a href=\"login\">Loggin in</a>');
define('_error404MessageNotLogged', 'This page may be private. If someone sent you this link, they may need to invite you to one of their boards or teams.');
define('_error500Title', 'Internal error');
define('_errorCheckIdentity', 'You are not <b>' . $_SESSION['user'] . '</b> ?<a href="logout">Logout</a>');

    // help.php

define('_helpTitle', 'Most popular items');
define('_helpMessage1', 'How to format your text on Trello');
define('_helpMessage2', 'Delete a board');
define('_helpMessage3', 'Loading CDN resources from Trello');
define('_helpMessage4', 'Use Trello with Google Calendar');
define('_helpMessage5', 'Trello Gold User Guide');
define('_helpMessage6', 'Archiving and deleting cards');
define('_helpMessage7', 'Using the Calendar Power-Up');
define('_helpMessage8', 'The Trello app for Slack');
define('_helpMessage9', 'How to use Trello like a pro');
define('_helpMessage10', 'Card creation by email');

    // image.php

define('_fileDoesNotMatch32x32', 'Your file does not correspond to the 150x150 dimension criterion');
define('_errorWhileDownloadingImage', 'An error occurred while uploading the image. Please try again.');
define('_importImage', 'Image import');
define('_image32x32Only', 'Only images of size 150x150 or less are allowed');
define('_image', 'Image');
define('_profilImage', 'Prfil image');

    // index.php

    // login.php

define('_loginFailed', 'Unable to connect, some information is false');
define('_loginEpitrello', 'Connection to the Epitrello site');
define('_loginEmail', 'Mail address');
define('_loginpassword', 'Password');
define('_connection', 'Connection');
define('_changePassword', 'Password change');
define('_createAccount', 'Create an account');

    // models.php

    // our_developpers.php

define('_ourDevelopers', 'Our Developers');

    // privacy_policy.php

    // recovery.php

define('_recoveryGiveEmail', 'Please enter your email address in the box below');
define('_codeToRecoverIs', 'The code for password change is');
define('_codeToChangePassword', 'Password change code');
define('_verificationCodeSent', 'The 5-character verification code has been sent to your email address');
define('_password', 'Password');
define('_newPassword', 'New Password');
define('_passwordVerification', 'Password verification');
define('_newPasswordVerification', 'New password verification');

    // register.php

define('_registerError', 'The account you wish to create already exists');
define('_registerAccount', 'Create an account on Epitrello');
define('_accountCreation', 'Account creation');

    // reset.php

define('_passwordChanged', 'Your password has been successfully changed');
define('_login', 'Login');
define('_codeError', 'Security error: the code indicated does not correspond to the code sent by email!');
define('_newTry', 'Retry');

    // teams.php

    // user_setting.php

define('_userSetting', 'User setting');
define('_changeProfilPicture', 'Change profile picture');
define('_changeInformations', 'Change informations');
define('_changeLang', 'Change language for :');
define('_deleteaccount', 'Delete account');
define('_closeAccount', 'Close my account');
define('_language', 'Language');

    // user.php

define('_sameBoards', 'Same boards');
define('_userNotFound', 'user not found.');