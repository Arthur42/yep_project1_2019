<html>
    <?php include "php/head.php" ?>
    <?php include 'php/session.php' ?>
    </head>
    <body>
        <?php include "php/header.php" ?>
        <?php
        if (isset($_GET["name"]) && $bdd->pseudoExist($_GET["name"])) {
            $user = $bdd->getUserByPseudo($_GET["name"]);
            $image_path = $user->getImage();
            if (!is_file('../' . $image_path))
                $image_path = 'img/user.png';
            $boards1 = $bdd->getUserBoardList($_SESSION['user']);
            $boards2 = $bdd->getUserBoardList($user->getEmail());
            $boards = array_intersect($boards1, $boards2);
            
            ?>
            <div class="user">
                <div class="user-header">
                    <div class="user-img"><img src="<?php echo $image_path; ?>" /></div>
                    <h1><?php echo $user->getName() . ' ' . $user->getSurname(); ?></h1>
                    <h2><?php echo '<a href="user?name=' . strtolower($user->getPseudo()) . '">@' . $user->getPseudo() . '</a>'; ?></h2>
                </div>
                <div class="user-body">
                    <h2><?php echo _sameBoards ?> :</h2>
                    <?php
                    foreach ($boards as $board) {
                        echo '<div><a href="board?board=' . $board . '">' . $board . '</a></div>';
                    }
                    ?>
                </div>
            </div>
        <?php
        } else {
        ?>
            <div class="user">
                <div class="user-header">
                    <h1><?php echo _userNotFound ?></h1>
                </div>
            </div>
        <?php
        }
        ?>
        <?php include "php/footer.php" ?>
        </footer>
    </body>
</html>