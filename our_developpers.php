<link rel="stylesheet" href="css/développeurs.css" type="text/css" />

<html>
    <?php include "php/head.php" ?>
    </head>
    <body>
        <?php include "php/header.php" ?>

        <div class='section-devs'>
        	<h1><?php echo _ourDevelopers; ?></h1>
        
	        <div class="dev-icon">
	        	<img src="img/arthur.barbier.jpg">
	        	<h2>Arthur Barbier</h2>
	        </div>
	        <div class="dev-icon">
	        	<img src="img/brian.merlin.jpg">
	        	<h2>Brian Merlin</h2>
	        </div>
	        <div class="dev-icon">
	        	<img src="img/tanguy.tournier.jpg">
	        	<h2>Tanguy Tournier</h2>
	        </div>
	        <div class="dev-icon">
	        	<img src="img/theo.scotton.jpg">
	        	<h2>Theo Scotton</h2>
	        </div>
	    </div>
        <?php include "php/footer.php" ?>
    </body>
</html>