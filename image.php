<html>
    <?php include "php/head.php" ?>
    <?php include 'php/session.php' ?>
    <?php
        $user = $bdd->getUserByEmail($_SESSION['user']);
    ?>
    <?php
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (!file_exists('./img/' . $_SESSION['user']))
                mkdir('./img/' . $_SESSION['user']);
            $target_dir = "img/" . $_SESSION['user'] . '/';
            $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
            $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            $image_width = $check[0];
            $image_height = $check[1];
            if ($image_width > 150 || $image_height > 150)
                echo _fileDoesNotMatch32x32;
            else {
                if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                    $image = $bdd->getImageByEmail($_SESSION['user']);
                    $file = '/yep_project1_2019/' . $target_file;
                    $toDelete = str_replace('/yep_project1_2019', '.', $image);
                    if ($image !== '/yep_project1_2019/img/user.png' && $image !== $file && file_exists($toDelete)) {
                        unlink($toDelete);
                    }
                    $bdd->updateImage($_SESSION['user'], $file);
                    echo '<script> window.location.href=\'/yep_project1_2019/user_setting\'</script>';
                } else {
                    echo _errorWhileDownloadingImage;
                }
            }
        }
    ?>
    </head>
    <body>
        <?php include "php/header.php" ?>
        <h1><?php echo _importImage; ?></h1>
        <p><?php echo _image32x32Only; ?></p><br />
        <form action='image' method='POST' enctype="multipart/form-data">
            <label for='image'><?php echo _image; ?>: </label>
            <input type='file' name='fileToUpload' id='image' placeholder='<?php echo _profilImage; ?>' /><br /><br />
            <input type='submit' name='submit' value='Valider' />
        </form>
        <br />
        <a href='/yep_project1_2019/user_setting'><?php echo _cancel; ?></a>
        <?php include "php/footer.php" ?>
        </footer>
    </body>
</html>