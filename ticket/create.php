<?php
    header('Content-Type: application/json');
    if ($_SERVER['REQUEST_METHOD'] !== 'POST' || !isset($_POST['title']) || !isset($_POST['c'])) {
        echo json_encode(array('status' => 'Error: bad method or data not set'));
    } else {
        include '../database/ticketDao.php';

        $ticketDao = new ticketDao;
        $title = $_POST['title'];
        $idcolumn = $_POST['c'];
        $ticket = $ticketDao->insert($title, $idcolumn);

        if ($ticket->getId() == -1)
            echo json_encode(array('status' => 'Error: ' . $ticketDao->getError()));
        else
            echo json_encode(array('status' => 'ok', 'id' => $ticket->getId(), 'idcolumn' => $ticket->getIdColumn()));
    }
?>