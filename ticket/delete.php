<?php
    header('Content-Type: application/json');
    if ($_SERVER['REQUEST_METHOD'] !== 'POST' || !isset($_POST['id']) || !isset($_POST['idcolumn']))
        echo json_encode(array('status' => 'Error: bad method or data not correctly set'));
    else {
        include '../database/ticketDao.php';
        include '../database/commentDao.php';

        $ticketDao = new ticketDao;
        $commentDao = new commentDao;
        $id = $_POST['id'];
        $idcolumn = $_POST['idcolumn'];

        if ($commentDao->deleteAll($id, $idcolumn) && $ticketDao->delete($id, $idcolumn)) {
            echo json_encode(array('status' => 'ok'));
        } else {
            $message = 'Error: ' . $ticketDao->getError() . ' ' . $commentDao->getError();
            echo json_encode(array('status' => $message));
        }
    }
?>