<?php
    header('Content-Type: application/json');
    if ($_SERVER['REQUEST_METHOD'] !== 'POST' || !isset($_POST['idticket']) || !isset($_POST['idcolumn']) || !isset($_POST['comment']))
        echo json_encode(array('status' => 'Error: bad request or data unset'));
    else {
        include '../database/commentDao.php';

        $commentDao = new commentDao;
        $idticket = $_POST['idticket'];
        $idcolumn = $_POST['idcolumn'];
        $comment = $_POST['comment'];
        $result = $commentDao->insert($comment, $idticket, $idcolumn);

        if ($result->id > -1) {
            $text = '<p>' . $result->comment . '<a class=\'delete_comment\' href=\'' . $result->id . '\'>X</a></p>';
            echo json_encode(array('status' => 'ok', 'comment' => $text));
        } else {
            echo json_encode(array('status' => 'Error: ' . $commentDao->getError()));
        }
    }
?>