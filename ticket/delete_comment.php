<?php
    header('Content-Type: application/json');
    if ($_SERVER['REQUEST_METHOD'] !== 'POST' || !isset($_POST['t']) || !isset($_POST['c']) || !isset($_POST['i']))
        echo json_encode(array('status' => 'Error: bad request or data unset'));
    else {
        include '../database/commentDao.php';

        $commentDao = new commentDao;
        $idticket = $_POST['t'];
        $idcolumn = $_POST['c'];
        $id = $_POST['i'];

        if ($commentDao->delete($id, $idticket, $idcolumn))
            echo json_encode(array('status' => 'ok'));
        else
            echo json_encode(array('status' => 'Error: ' . $commentDao->getError()));
    }
?>