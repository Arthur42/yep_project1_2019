<?php
    header('Content-Type: application/json');
    if ($_SERVER['REQUEST_METHOD'] != 'POST' || !isset($_POST['id']) || !isset($_POST['idcolumn']))
        echo json_encode(array('status' => 'Error: bas request or data unset'));
    else {
        include '../database/ticketDao.php';
        include '../database/commentDao.php';

        $ticketDao = new ticketDao;
        $commentDao = new commentDao;
        $id = $_POST['id'];
        $idcolumn = $_POST['idcolumn'];
        $ticket = $ticketDao->getTicket($id, $idcolumn);
        $comments = $commentDao->getAllByIds($id, $idcolumn);

        if ($ticket->getId() > -1) {
            $content = '' .
            '<div class=\'ticket_background\'></div>' .
            '<div class=\'modifier\'>' .
                '<input id=\'modify_title\' type=\'text\' value=\'' . $ticket->getTitle() . '\' /><br /><br />' .
                '<textarea id=\'modify_description\' placeholder=\'ajouter une description\' cols=\'100\'>' . $ticket->getDescription() . '</textarea><br />' .
                '<p id=\'comments\'>Commentaires<p>';
            foreach ($comments as $c)
                $content .= '<p>' . $c->comment . '<a class=\'delete_comment\' href=\'' . $c->id . '\'>X</a></p>';
            $content .='<input type=\'text\' id=\'comment_text\' placeholder=\'ajouter un commentaire\' size=\'100\' /><button class=\'add_comment\'>ok</button>' .
                '<br /><br /><button class=\'delete_button\'><i class="icon-trash"></i></button>' .
            '</div>';
            echo json_encode(array('status' => 'ok', 'content' => $content));
        } else
            echo json_encode(array('status' => 'Error: ticket not found. ' . $ticketDao->getError()));
    }
?>