<?php
    if ($_SERVER['REQUEST_METHOD'] !== 'POST' || !isset($_POST['title']) || !isset($_POST['id']) || !isset($_POST['idcolumn']))
        echo 'Error';
    else {
        include '../database/ticketDao.php';

        $ticketDao = new ticketDao;
        $title = $_POST['title'];
        $id = $_POST['id'];
        $idcolumn = $_POST['idcolumn'];

        header('Content-Type: application/json');
        if ($ticketDao->updateTitle($title, $id, $idcolumn))
            echo json_encode(array('status' => 'ok', 'id' => $id, 'idcolumn' => $idcolumn));
        else
            echo json_encode(array('status' => 'Error ' . $ticketDao->getError()));
    }

?>