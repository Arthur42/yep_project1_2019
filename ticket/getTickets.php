<?php
    header('Content-Type: application/json');
    if ($_SERVER['REQUEST_METHOD'] !== 'POST' || !isset($_POST['idcolumn']))
        echo json_encode(array('status' => 'Error: bad request or unset data'));
    else {
        include '../database/ticketDao.php';

        $ticketDao = new ticketDao;
        $idcolumn = $_POST['idcolumn'];
        $tickets = $ticketDao->getAllByColumnId($idcolumn);
       
        if (sizeof($tickets) > 0)
            echo json_encode(array('status' => 'ok', 'tickets' => $tickets));
        else 
            echo json_encode(array('status' => 'Error: ' . $ticketDao->getError()));
    }
?>