<?php
    header('Content-Type: application/json');
    if ($_SERVER['REQUEST_METHOD'] !== 'POST' || !isset($_POST['description']) || !isset($_POST['id']) || !isset($_POST['idcolumn']))
        echo json_encode(array('status' => 'Error: bad request or data unset'));
    else {
        include '../database/ticketDao.php';

        $ticketDao = new ticketDao;
        $id = $_POST['id'];
        $idcolumn = $_POST['idcolumn'];
        $description = $_POST['description'];

        if ($ticketDao->updateDescription($description, $id, $idcolumn))
            echo json_encode(array('status' => 'ok'));
        else
            echo json_encode(array('status' => 'Error: ' . $ticketDao->getError()));
    }
?>