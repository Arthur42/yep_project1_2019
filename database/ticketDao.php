<?php

include 'bean/Ticket.php';

class ticketDao
{
    private $_bdd = null;
    private $_error = null;

    public function __construct() {
        try {
            $this->_bdd = new PDO('mysql:host=localhost:3308;dbname=epitrello;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
        }
    }

    public function getTicket($id, $idcolumn) {
        $t = new Ticket;
        $request = $this->_bdd->prepare('SELECT * FROM ticket WHERE id=:id AND idcolumn=:idcolumn');

        try {
            $request->execute(array('id' => $id, 'idcolumn' => $idcolumn));
            $data = $request->fetch();
            $t->setId($data['ID']);
            $t->setIdColumn($data['IDCOLUMN']);
            $t->setTitle($data['TITLE']);
            $t->setDescription($data['DESCRIPTION']);
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
        } finally {
            $request->closeCursor();
        }
        return $t;
    }

    public function getAllByColumnId($columnid) {
        $result = array();
        $request = $this->_bdd->prepare('SELECT * FROM ticket WHERE idcolumn=:columnid ORDER BY ID');
        $i = 0;

        try {
            $request->execute(array('columnid' => $columnid));
            while ($data = $request->fetch()) {
                $t = new Ticket;

                $t->setId($data['ID']);
                $t->setIdColumn($data['IDCOLUMN']);
                $t->setTitle($data['TITLE']);
                $t->setDescription($data['DESCRIPTION']);
                $result[$i++] = $t;
            }
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
        } finally {
            $request->closeCursor();
        }
        return $result;
    }

    public function insert($title, $idcolumn) {
        $insert = $this->_bdd->prepare('INSERT INTO ticket (`id`, `idcolumn`, `title`) SELECT MAX(id) + 1, :c, :t FROM ticket WHERE idcolumn=:c');
        $select = $this->_bdd->prepare('SELECT MAX(ID) as ID FROM ticket WHERE idcolumn=:c');
        $ticket = new Ticket;

        try {
            $insert->execute(array('t' => $title, 'c' => $idcolumn));
            $select->execute(array('c' => $idcolumn));
            $data = $select->fetch();
            $ticket->setId($data['ID']);
            $ticket->setIdColumn($idcolumn);
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
        } finally {
            $insert->closeCursor();
            $select->closeCursor();
        }
        return $ticket;
    }

    public function updateTitle($title, $id, $idcolumn) {
        if ($title == null) {
            $this->_error = 'title can\'t be null';
            return false;
        }
        $request = $this->_bdd->prepare('UPDATE ticket SET title=:title WHERE id=:id AND idcolumn=:idcolumn');
        $status = true;

        try {
            $request->execute(array(
                'title' =>$title,
                'id' => $id,
                'idcolumn' => $idcolumn
            ));
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
            $status = false;
        } finally {
            $request->closeCursor();
        }
        return $status;
    }

    public function updateDescription($description, $id, $idcolumn) {
        if ($description == null) {
            $this->_error = 'description can\' be null';
            return false;
        }
        $request = $this->_bdd->prepare('UPDATE ticket SET description=:d WHERE id=:id AND idcolumn=:idcolumn');
        $status = true;

        try {
            $request->execute(array(
                'd' => $description,
                'id' => $id,
                'idcolumn' => $idcolumn
            ));
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
            $status = false;
        } finally {
            $request->closeCursor();
        }
        return $status;
    }

    public function delete($id, $idcolumn) {
        $request = $this->_bdd->prepare('DELETE FROM ticket WHERE id=:id AND idcolumn=:idcolumn');
        $status = true;

        try {
            $request->execute(array(
                'id' => $id,
                'idcolumn' => $idcolumn
            ));
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
            $status = false;
        } finally {
            $request->closeCursor();
        }
        return $status;
    }

    public function getError() {
        return $this->_error;
    }
}
?>