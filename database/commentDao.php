<?php

include 'bean/Comment.php';

class commentDao
{
    private $_bdd = null;
    private $_error = null;

    public function __construct() {
        try {
            $this->_bdd = new PDO('mysql:host=localhost:3308;dbname=epitrello;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
        }
    }

    public function insert($comment, $idticket, $idcolumn) {
        $status = true;
        $insert = $this->_bdd->prepare('INSERT INTO comments (`ID`, `IDTICKET`, `IDCOLUMN`, `COMMENT`) SELECT MAX(ID) + 1, :t, :c, :comment FROM comments WHERE idcolumn=:c AND idticket=:t');
        $select = $this->_bdd->prepare('SELECT * FROM comments WHERE id=(SELECT MAX(id) FROM comments) AND idticket=:t AND idcolumn=:c');
        $result = new Comment;

        try {
            $insert->execute(array(
                't' => $idticket,
                'c' => $idcolumn,
                'comment' => $comment
            ));
            $select->execute(array(
                't' => $idticket,
                'c' => $idcolumn
            ));
            $data = $select->fetch();
            $result->id = $data['ID'];
            $result->idcolumn = $data['IDCOLUMN'];
            $result->idticket = $data['IDTICKET'];
            $result->comment = $data['COMMENT'];
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
        } finally {
            $insert->closeCursor();
            $select->closeCursor();
        }
        return $result;
    }

    public function getAllByIds($idticket, $idcolumn) {
        $request = $this->_bdd->prepare('SELECT * FROM comments WHERE idticket=:t AND idcolumn=:c ORDER BY ID DESC');
        $result = array();
        $i = 0;

        try {
            $request->execute(array('t' => $idticket, 'c' => $idcolumn));
            while ($data = $request->fetch()) {
                $c = new Comment;

                $c->id = $data['ID'];
                $c->idticket = $data['IDTICKET'];
                $c->idcolumn = $data['IDCOLUMN'];
                $c->comment = $data['COMMENT'];
                $result[$i++] = $c;
            }
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
        } finally {
            $request->closeCursor();
        }
        return $result;
    }

    public function delete($id, $idticket, $idcolumn) {
        $request = $this->_bdd->prepare('DELETE FROM comments WHERE id=:i AND idticket=:t AND idcolumn=:c');
        $status = true;

        try {
            $request->execute(array(
                'i' => $id,
                't' => $idticket,
                'c' => $idcolumn
            ));
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
            $status = false;
        } finally {
            $request->closeCursor();
        }
        return $status;
    }

    public function deleteAll($idticket, $idcolumn) {
        $request = $this->_bdd->prepare('DELETE FROM comments WHERE idticket=:t AND idcolumn=:c');
        $status = true;

        try {
            $request->execute(array(
                't' => $idticket,
                'c' => $idcolumn
            ));
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
            $status = false;
        } finally {
            $request->closeCursor();
        }
        return $status;
    }

    public function getError() {
        return $this->_error;
    }
}

?>