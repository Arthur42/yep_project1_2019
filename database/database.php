<?php

include 'bean/User.php';

class Database
{
    private $_bdd = null;

    public function __construct() {
        try {
            $this->_bdd = new PDO('mysql:host=localhost:3308;dbname=epitrello;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
        }
    }

    public function getUsers() {
        return $this->_bdd->query('SELECT * FROM user');
    }
    
    public function getUserByPseudo($pseudo) {
        $user = new User;
        if ($pseudo == null)
            return $user;
        $request = $this->_bdd->prepare('SELECT * FROM user WHERE PSEUDO = :pseudo');
        try {
            $request->execute(array(':pseudo' => $pseudo));
            $data = $request->fetch();
            $user->setName($data['NOM']);
            $user->setSurname($data['PRENOM']);
            $user->setEmail($data['EMAIL']);
            $user->setPseudo($data['PSEUDO']);
            $user->setImage($data['IMAGE']);
            $user->setLanguage($data['LANGUAGE']);
        } catch (Execption $e) {
            echo 'Error ' . $e->getMessage();
        } finally {
            $request->closeCursor();
        }
        return $user;
    }

    public function getUserByEmail($email) {
        $user = new User;
        $request = $this->_bdd->prepare('SELECT * FROM user WHERE email = :email');

        if ($email == null)
            return $user;
        try {
            $request->execute(array(':email' => $email));
            $data = $request->fetch();
            $user->setName($data['NOM']);
            $user->setSurname($data['PRENOM']);
            $user->setEmail($data['EMAIL']);
            $user->setPseudo($data['PSEUDO']);
            $user->setImage($data['IMAGE']);
            $user->setLanguage($data['LANGUAGE']);
        } catch (Execption $e) {
            echo 'Error ' . $e->getMessage();
        } finally {
            $request->closeCursor();
        }
        return $user;
    }

    public function getImageByEmail($email) {
        $image = '';
        if ($email == null)
            return $image;
        $request = $this->_bdd->prepare('SELECT image FROM user WHERE email=:email');
        
        try {
            $request->execute(array('email' => $email));
            $data = $request->fetch();
            $image = $data['image'];
        } catch (Execption $e) {
            echo 'Error ' . $e->getMessage();
        } finally {
            $request->closeCursor();
        }
        return $image;
    }

    public function userExist($email, $password) {
        if ($email == null || $password == null)
            return false;
        $status = false;
        $request = $this->_bdd->prepare('SELECT password FROM user WHERE email = :email');

        try {
            $request->execute(array(':email' => $email));
            if ($data = $request->fetch()) {
                if (password_verify($password, $data['password']))
                    $status = true;
            }
        } catch (Execption $e) {
            echo 'Error ' . $e->getMessage();
        } finally {
            $request->closeCursor();
        }
        return $status;
    }

    public function emailExist($email) {
        if ($email == null)
            return false;
        $status = false;
        $request = $this->_bdd->prepare('SELECT * FROM user WHERE email = :email');

        try {
            $request->execute(array(':email' => $email));
            if ($data = $request->fetch()) {
                $status = true;
            }
        } catch (Execption $e) {
            echo 'Error ' . $e->getMessage();
        } finally {
            $request->closeCursor();
        }
        return $status;
    }

    public function insert($name, $surname, $email, $password) {
        if ($email == null || $surname == null || $name == null || $password == null)
            return false;
        $status = true;
        $request = $this->_bdd->prepare('INSERT INTO user (`NOM`, `PRENOM`, `EMAIL`, `PASSWORD`, `PSEUDO`, `IMAGE`, `LANGUAGE`) SELECT :name, :surname, :email, :password, :pseudo, \'/yep_project1_2019/img/user.png\', \'fr\' FROM user');
        try {
            $request->execute(array(
                'name' => $name, 
                'surname' => $surname, 
                'email' => $email, 
                'password' => $password,
                'pseudo' => $surname
                ));
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
            $status = false;
        } finally {
            $request->closeCursor();
        }
        return $status;
    }

    public function isVerified($email) {
        if ($email == null)
            return false;
        $status = false;
        $request = $this->_bdd->prepare('SELECT verified FROM user WHERE email = :email');
        try {
            $request->execute(array('email' => $email));
            $data = $request->fetch();
            if ($data)
                $status = $data['verified'];
        } catch (Exeception $e) {
            echo $e->getMessage();
        } finally {
            $request->closeCursor();
        }
        return $status;
    }

    public function updateVerificationCode($email, $code) {
        if ($email == null || $code == null)
            return false;
        $status = true;
        $request = $this->_bdd->prepare('UPDATE user SET code=:code WHERE email=:email');
        try {
            $request->execute(array('email' => $email, 'code' => $code));
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
            $status = false;
        } finally {
            $request->closeCursor();
        }
        return $status;
    }

    public function codeVerification($email, $code) {
        if ($email == null || $code == null)
            return false;
        $status = false;
        $request = $this->_bdd->prepare('SELECT code FROM user WHERE email = :email');

        try {
            $request->execute(array(':email' => $email));
            if ($data = $request->fetch()) {
                if (password_verify($code, $data['code']))
                    $status = true;
            }
        } catch (Execption $e) {
            echo 'Error ' . $e->getMessage();
        } finally {
            $request->closeCursor();
        }
        return $status;
    }

    public function updateVerificationChecked($email) {
        if ($email == null)
            return false;
        $status = true;
        $request = $this->_bdd->prepare('UPDATE user SET verified=1 WHERE email=:email');
        try {
            $request->execute(array('email' => $email));
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
            $status = false;
        } finally {
            $request->closeCursor();
        }
        return $status;
    }

    public function updatePassword($email, $password) {
        if ($email == null || $password == null)
            return false;
        $status = true;
        $request = $this->_bdd->prepare('UPDATE user SET password=:password WHERE email=:email');
        $password_hash = password_hash($password, PASSWORD_DEFAULT);

        try {
            $request->execute(array('email' => $email, 'password' => $password_hash));
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
            $status = false;
        } finally {
            $request->closeCursor();
        }
        return $status;
    }

    public function updateInformations($name, $surname, $pseudo, $email) {
        if ($name == null || $surname == null || $pseudo == null || $email == null)
            return false;
        $status = true;
        $request = $this->_bdd->prepare('UPDATE user SET nom=:name, prenom=:surname, pseudo=:pseudo WHERE email=:email');

        try {
            $request->execute(array(
                'name' => $name,
                'surname' => $surname,
                'pseudo' => $pseudo,
                'email' => $email
            ));
        } catch (Exception $e) {
            echo 'Error: ' . $e->getMessage();
            $status = false;
        } finally {
            $request->closeCursor();
        }
        return $status;
    }
    
    private function getUserID($email) {
        if ($email == null)
            return 0;
        $userID = 0;
        $request = $this->_bdd->prepare('SELECT ID FROM user WHERE EMAIL = :email');
        try {
            $request->execute(array('email' => $email));
            $data = $request->fetch();
            $userID = $data['ID'];
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
            $userID = 0;
        } finally {
            $request->closeCursor();
        }
        return $userID;
    }
    
    public function boardExist($boardName, $email) {
        $userID = $this->getUserID($email);
        if ($boardName == null || $userID == 0)
            return false;
        $status = false;
        $request = $this->_bdd->prepare('SELECT NAME FROM board
        INNER JOIN board_user
            ON board.ID = board_user.IDBOARD
            WHERE board_user.IDUSER = :userID AND board.NAME = :name');
        try {
            $request->execute(array('userID' => $userID, 'name' => $boardName));
            if ($request->fetch()) {
                $status = true;
            }
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
            $status = false;
        } finally {
            $request->closeCursor();
        }
        return $status;
    }
    
    public function updateImage($email, $image) {
        if ($email == null || $image == null)
            return false;
        $status = true;
        $request = $this->_bdd->prepare('UPDATE user SET image=:image WHERE email=:email');

        try {
            $request->execute(array('image' => $image, 'email' => $email));
        } catch (Execption $e) {
            echo 'Error ' . $e->getMessage();
            $status = false;
        } finally {
            $request->closeCursor();
        }
        return $status;
    }
    
    public function columnExist($boardName, $email, $columnName) {
        $userID = $this->getUserID($email);
        if ($boardName == null || $userID == 0 || $columnName == null)
            return false;
        $status = false;
        $request = $this->_bdd->prepare('SELECT board_column.ID
        FROM board_column
        INNER JOIN board
            ON board_column.IDBOARD = board.ID
        INNER JOIN board_user
            ON board.ID = board_user.IDBOARD
            WHERE board_column.NAME = :columnName AND board_user.IDUSER = :userID AND board.NAME = :boardName');
        try {
            $request->execute(array('userID' => $userID, 'boardName' => $boardName, 'columnName' => $columnName));
            if ($request->fetch()) {
                $status = true;
            }
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
            $status = false;
        } finally {
            $request->closeCursor();
        }
        return $status;
    }

    public function updateLanguage($email, $language) {
        if ($email == null || $language == null) {
            return false;
        }
        $status = true;
        $request = $this->_bdd->prepare('UPDATE user SET language=:language WHERE email=:email');

        try {
            $request->execute(array('language' => $language, 'email' => $email));
        } catch (Execption $e) {
            echo 'Error ' . $e->getMessage();
            $status = false;
        } finally {
            $request->closeCursor();
        }
        return $status;
    }

    public function destroyUser($email) {
        if ($email == null)
            return false;
        $status = true;
        $request = $this->_bdd->prepare('DELETE FROM user WHERE email=:email');

        try {
            $request->execute(array('email' => $email));
        } catch (Exception $e) {
            echo 'Error: ' . $e->getMessage();
            $status = false;
        } finally {
            $request->closeCursor();
        }
        return $status;
    }
    
    public function createBoard($boardName, $email) {
        $userID = $this->getUserID($email);
        if ($boardName == null || $userID == 0 || $this->boardExist($boardName, $userID) === true)
            return false;
        $status = true;
        $request1 = $this->_bdd->prepare('INSERT INTO board (`NAME`) SELECT :name');
        $request2 = $this->_bdd->prepare('INSERT INTO board_user (`IDUSER`, `IDBOARD`) SELECT :iduser, :idboard');
        try {
            $request1->execute(array('name' => $boardName));
            $last_id = $this->_bdd->lastInsertId();
            $request2->execute(array('iduser' => $userID, 'idboard' => $last_id));
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
            $status = false;
        } finally {
            $request1->closeCursor();
            $request2->closeCursor();
        }
        return $status;
    }
    
    public function addColumn($boardName, $email, $columnName) {
        $userID = $this->getUserID($email);
        if ($boardName == null || $userID == 0 || $this->columnExist($boardName, $userID, $columnName) === true)
            return false;
        $request1 = $this->_bdd->prepare('SELECT board.ID
        FROM board
        INNER JOIN board_user
            ON board.ID = board_user.IDBOARD
            WHERE board.NAME = :name AND board_user.IDUSER = :iduser');
        $request2 = $this->_bdd->prepare('INSERT INTO board_column (`IDBOARD`, `NAME`, `POSITION`) SELECT :idboard, :name, (SELECT MAX(POSITION) + 1
                                FROM board_column
                                WHERE IDBOARD = :idboard)');
        try {
            $request1->execute(array('name' => $boardName, 'iduser' => $userID));
            $boardID = 0;
            if ($data = $request1->fetch()) {
                $boardID = $data["ID"];
            }
            $request2->execute(array('idboard' => $boardID, 'name' => $columnName));
            $status = true;
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
            $status = false;
        } finally {
            $request1->closeCursor();
            $request2->closeCursor();
        }
        return $status;
    }
    
    public function getBoardColumn($boardName, $email) {
        $userID = $this->getUserID($email);
        if ($boardName == null || $userID == 0)
            return false;
        $res;
        $request = $this->_bdd->prepare('SELECT
            board_column.NAME,
            board_column.ID,
            board_column.ARCHIVED
        FROM board_column
        INNER JOIN board
            ON board_column.IDBOARD = board.ID
        INNER JOIN board_user
            ON board.ID = board_user.IDBOARD
            WHERE board_user.IDUSER = :iduser AND board.NAME = :name
        ORDER BY board_column.POSITION');
        try {
            $request->execute(array('iduser' => $userID, 'name' => $boardName));
            while ($data = $request->fetch()) {
                $res[] = array('id' => $data["ID"],
                               'name' => $data["NAME"],
                               'archive' => $data["ARCHIVED"]);
            }
            if (isset($res))
                $res = json_encode($res);
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
            $res = false;
        } finally {
            $request->closeCursor();
        }
        if (isset($res))
            return $res;
        else
            return false;
    }
    
    public function changeColumnPosition($boardName, $email, $oldPos, $newPos) {
        $userID = $this->getUserID($email);
        if ($boardName == null || $userID == 0)
            return false;
        $status = true;
        $opNewP = ($oldPos < $newPos) ? "<=" : ">=";
        $opOldP = ($oldPos < $newPos) ? ">" : "<";
        $op = ($oldPos < $newPos) ? "-" : "+";
        $request1 = $this->_bdd->prepare('UPDATE board_column
        INNER JOIN board
            ON board_column.IDBOARD = board.ID
        INNER JOIN board_user
            ON board.ID = board_user.IDBOARD
        SET board_column.POSITION = :newposition
        WHERE board_user.IDUSER = :iduser
            AND board.ID = board_column.IDBOARD
            AND board_column.POSITION = :oldposition');
        $request2 = $this->_bdd->prepare('UPDATE board_column
        INNER JOIN board
            ON board_column.IDBOARD = board.ID
        INNER JOIN board_user
            ON board.ID = board_user.IDBOARD
        SET board_column.POSITION = board_column.POSITION ' . $op . ' 1
        WHERE board_user.IDUSER = :iduser
            AND board.ID = board_column.IDBOARD
            AND board_column.POSITION ' . $opNewP . ' :newposition
            AND board_column.POSITION ' . $opOldP . ' :oldposition');
        try {
            $request1->execute(array('iduser' => $userID,
                                    'oldposition' => $oldPos,
                                    'newposition' => 0));
            $request2->execute(array('iduser' => $userID,
                                    'oldposition' => $oldPos,
                                    'newposition' => $newPos));
            $request1->execute(array('iduser' => $userID,
                                    'oldposition' => 0,
                                    'newposition' => $newPos));
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
            $status = false;
        } finally {
            $request1->closeCursor();
            $request2->closeCursor();
        }
        return $status;
    }
    
    public function getBoardContent($boardName, $email) {
        $userID = $this->getUserID($email);
        if ($boardName == null || $userID == 0)
            return false;
        $res;
        $request = $this->_bdd->prepare('SELECT
            ticket.ID as ticketID,
            ticket.TITLE,
            ticket.DESCRIPTION,
            board_column.ID as columnID
        FROM ticket
        INNER JOIN board_column
            ON ticket.IDCOLUMN = board_column.ID
        INNER JOIN board
            ON board_column.IDBOARD = board.ID
        INNER JOIN board_user
            ON board.ID = board_user.IDBOARD
            WHERE board_user.IDUSER = :iduser AND board.NAME = :name
        ORDER BY ticket.IDCOLUMN AND ticket.ID DESC');
        try {
            $request->execute(array('iduser' => $userID,
                                    'name' => $boardName));
            while ($data = $request->fetch()) {
                $res[] = array("columnID" => $data["columnID"],
                               "ticketID" => $data["ticketID"],
                               "title" => $data["TITLE"],
                               "description" => $data["DESCRIPTION"]);
            }
            if (isset($res))
                $res = json_encode($res);
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
            $res = false;
        } finally {
            $request->closeCursor();
        }
        if (isset($res))
            return $res;
        else
            return false;
    }
    
    public function getUserBoards($email) {
        $userID = $this->getUserID($email);
        if ($userID == 0)
            return false;
        $res = false;
        $request = $this->_bdd->prepare('SELECT board.NAME
        FROM board
        INNER JOIN board_user
            ON board.ID = board_user.IDBOARD
        WHERE board_user.IDUSER = :iduser');
        try {
            $request->execute(array('iduser' => $userID));
            while ($data = $request->fetch()) {
                $res[] = $data["NAME"];
            }
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
            $res = false;
        } finally {
            $request->closeCursor();
        }
        return $res;
    }
    
    public function archiveColumn($boardName, $email, $columnID, $archive) {
        $userID = $this->getUserID($email);
        if ($boardName == null || $userID == 0 || $columnID <= 0)
            return false;
        $status = false;
        $request = $this->_bdd->prepare('UPDATE board_column
        INNER JOIN board
            ON board_column.IDBOARD = board.ID
        INNER JOIN board_user
            ON board.ID = board_user.IDBOARD
        SET board_column.ARCHIVED = :archive
        WHERE board_user.IDUSER = :iduser
            AND board.NAME = :name
            AND board_column.ID = :columnid');
        try {
            $request->execute(array('iduser' => $userID,
                                    'name' => $boardName,
                                    'columnid' => $columnID,
                                    'archive' => $archive));
            $status = true;
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
            $status = false;
        } finally {
            $request->closeCursor();
        }
        return $status;
    }
    
    public function getUserLang($email) {
        if ($email == null)
            return false;
        $lang = false;
        $request = $this->_bdd->prepare('SELECT language
        FROM user
        WHERE EMAIL = :email');
        try {
            $request->execute(array('email' => $email));
            if ($data = $request->fetch()) {
                $lang = $data['language'];
            }
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
            $lang = false;
        } finally {
            $request->closeCursor();
        }
        return $lang;
    }
    
    public function pseudoExist($pseudo) {
        if ($pseudo == null)
            return false;
        $status = false;
        $request = $this->_bdd->prepare('SELECT PSEUDO
        FROM user
        WHERE PSEUDO = :pseudo');
        try {
            $request->execute(array('pseudo' => $pseudo));
            if ($data = $request->fetch()) {
                $status = true;
            }
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
            $status = false;
        } finally {
            $request->closeCursor();
        }
        return $status;
    }
    
    public function getUserBoardList($email) {
        if ($email == null)
            return false;
        $boards;
        $request = $this->_bdd->prepare('SELECT board.NAME
        FROM board
        INNER JOIN board_user
            ON board.ID = board_user.IDBOARD
        INNER JOIN user
            ON board_user.IDUSER = user.ID
        WHERE user.EMAIL = :email');
        try {
            $request->execute(array('email' => $email));
            $i = 0;
            while ($data = $request->fetch()) {
                $boards[] = $data["NAME"];
                $i++;
            }
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
            $boards = false;
        } finally {
            $request->closeCursor();
        }
        return $boards;
    }
    
    public function getUserTicketList($email) {
        if ($email == null)
            return false;
        $tickets;
        $request = $this->_bdd->prepare('SELECT board.NAME, ticket.TITLE
        FROM ticket
        INNER JOIN board_column
            ON ticket.IDCOLUMN = board_column.ID
        INNER JOIN board
            ON board_column.IDBOARD = board.ID
        INNER JOIN board_user
            ON board.ID = board_user.IDBOARD
        INNER JOIN user
            ON board_user.IDUSER = user.ID
        WHERE user.EMAIL = :email');
        try {
            $request->execute(array('email' => $email));
            $i = 0;
            while ($data = $request->fetch()) {
                $tickets[] = array("boardName" => $data["NAME"],
                                  "ticketName" => $data["TITLE"]);
                $i++;
            }
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
            $tickets = false;
        } finally {
            $request->closeCursor();
        }
        return $tickets;
    }
}
?>
