<?php
    class Ticket {
        public $_id = -1;
        public $_idcolumn = -1;
        public $_title = null;
        public $_description = null;

        public function getId() {
            return $this->_id;
        }

        public function setId($id) {
            $this->_id = $id;
        }

        public function getIdColumn() {
            return $this->_idcolumn;
        }

        public function setIdColumn($idcolumn) {
            $this->_idcolumn = $idcolumn;
        }

        public function getTitle() {
            return $this->_title;
        }

        public function setTitle($title) {
            $this->_title = $title;
        }

        public function getDescription() {
            return $this->_description;
        }

        public function setDescription($description) {
            $this->_description = $description;
        }
    }
?>