-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3308
-- Généré le :  lun. 13 avr. 2020 à 14:21
-- Version du serveur :  8.0.18
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `epitrello`
--

-- --------------------------------------------------------

--
-- Structure de la table `board`
--

DROP TABLE IF EXISTS `board`;
CREATE TABLE IF NOT EXISTS `board` (
  `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `board`
--

INSERT INTO `board` (`ID`, `NAME`) VALUES
(1, 'dddd');

-- --------------------------------------------------------

--
-- Structure de la table `board_column`
--

DROP TABLE IF EXISTS `board_column`;
CREATE TABLE IF NOT EXISTS `board_column` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IDBOARD` int(11) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `ARCHIVED` tinyint(1) NOT NULL,
  `POSITION` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `board_column`
--

INSERT INTO `board_column` (`ID`, `IDBOARD`, `NAME`, `ARCHIVED`, `POSITION`) VALUES
(1, 1, 'first', 0, 1),
(2, 1, 'second', 0, 2),
(6, 1, 'sd', 1, 3),
(5, 1, 'er', 1, 4);

-- --------------------------------------------------------

--
-- Structure de la table `board_user`
--

DROP TABLE IF EXISTS `board_user`;
CREATE TABLE IF NOT EXISTS `board_user` (
  `IDUSER` int(11) NOT NULL,
  `IDBOARD` int(11) NOT NULL,
  UNIQUE KEY `IDUSER` (`IDUSER`,`IDBOARD`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `board_user`
--

INSERT INTO `board_user` (`IDUSER`, `IDBOARD`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ticket`
--

DROP TABLE IF EXISTS `ticket`;
CREATE TABLE IF NOT EXISTS `ticket` (
  `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `IDCOLUMN` int(10) UNSIGNED NOT NULL,
  `TITLE` varchar(255) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  UNIQUE KEY `ID` (`ID`,`IDCOLUMN`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `ticket`
--

INSERT INTO `ticket` (`ID`, `IDCOLUMN`, `TITLE`, `DESCRIPTION`) VALUES
(1, 1, 'ticket', 'description du ticker'),
(2, 1, 'jeux', 'ceci est un commentaire');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `NOM` varchar(255) NOT NULL,
  `PRENOM` varchar(255) NOT NULL,
  `EMAIL` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `PASSWORD` varchar(255) NOT NULL,
  `ID` int(4) UNSIGNED NOT NULL AUTO_INCREMENT,
  `VERIFIED` tinyint(1) NOT NULL,
  `CODE` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `PSEUDO` varchar(100) NOT NULL,
  `IMAGE` varchar(255) NOT NULL,
  `LANGUAGE` varchar(4) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `EMAIL` (`EMAIL`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`NOM`, `PRENOM`, `EMAIL`, `PASSWORD`, `ID`, `VERIFIED`, `CODE`, `PSEUDO`, `IMAGE`, `LANGUAGE`) VALUES
('Tournier', 'Tanguy', 'tanguy.tournier@epitech.eu', '$2y$10$0GyEIKZWgyEe.JsVaHsRP.9gvGOVQstVXqwTYEDaVh1Zvn8C0SAHS', 1, 1, '$2y$10$Vts7qGKZy9a62obGygx63O5wJ0CBzpOSaBwZM1xGGY8w2q5axrJYG', 'Tanguy', '/path/to/image', 'fr');
INSERT INTO `user` (`NOM`, `PRENOM`, `EMAIL`, `PASSWORD`, `ID`, `VERIFIED`, `CODE`, `PSEUDO`, `IMAGE`, `LANGUAGE`) VALUES
('arthur', 'barbier', 'ab@gmail.com', '$2y$10$vNd0K4i23uqZMoUFh/EgveAQmWkpoHzAbpENaIasZQi6vl0Zozpg.', 2, 1, NULL, 'Petshop', '/path/to/image', 'fr');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
