<html>
    <?php include "php/head.php" ?>
    <script src="js/password_verification.js"></script>
    </head>
    <body>
        <?php include "php/header.php" ?>
        <?php
            if ($_SERVER['REQUEST_METHOD'] === 'GET') {
        ?>
        <h1><?php echo _changePassword; ?></h1>
        <p><?php echo _recoveryGiveEmail; ?></p><br />
        <form action='recovery'  method='POST'>
            <label for='email'><?php echo _loginEmail; ?>: </label>
            <input type='text' size='100' length='100' id='email' name='email' placeholder='<?php echo _loginEmail; ?>' />
            <input type='submit' value='<?php echo _confirm; ?>' />
        </form>
        <?php
            } else if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                include 'usefull/random_string.php';

                $email = $_POST['email'];
                $code = randomString(5);
                if ($bdd->emailExist($email)) {
                    include 'usefull/mail.php';

                    $msg = _codeToRecoverIs . ': ' . $code;
                    $bdd->updateVerificationCode($email, password_hash($code, PASSWORD_DEFAULT));
                    $mail = new Mail;
                    $mail->sendMail($email, _codeToChangePassword, $msg);
        ?>
                    <h1><?php echo _changePassword; ?></h1>
                    <p><?php echo _verificationCodeSent; ?></p><br />
                    <form action='reset' method='POST' id='form'>
                        <label for='password'><?php echo _password; ?>: </label>
                        <input type='password' name='password' id='password' size='50' lenght='50' placeholder='<?php echo _newPassword; ?>' />
                        <br />
                        <label for='password_check'><?php echo _passwordVerification; ?>: </label>
                        <input type='password' name='password_check' id='password_check' size='50' lenght='50' placeholder='<?php echo _newPasswordVerification; ?>' />
                        <br />
                        <label for='code'><?php echo _verificationCode; ?></label>
                        <input type='text' name='code' id='code' size='5' lenght='5' placeholder='<?php echo _code; ?>' />
                        <br />
                        <?php echo '<input type=\'hidden\' name=\'email\' value=\'' . $email . '\' />'; ?>
                        <input type='submit' value='<?php echo _changePassword; ?>' />
                    </form>
        <?php
                } else {
        ?>
                    <h1><?php echo _changePassword; ?></h1>
                    <p><?php echo _recoveryGiveEmail; ?></p><br />
                    <form action='recovery'  method='POST'>
                        <label for='email'><?php echo _loginEmail; ?>: </label>
                        <input type='text' size='100' length='100' id='email' name='email' placeholder='<?php echo _loginEmail; ?>' />
                        <input type='submit' value='<?php echo _confirm; ?>' />
                    </form>
                    <br />
                    <p style='color: red'>L'adresse mail indiqué n'existe pas dans la base de données</p>
        <?php
                }
            }
        ?>
        <?php include "php/footer.php" ?>
        </footer>
    </body>
</html>