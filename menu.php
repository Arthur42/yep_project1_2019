<link rel="stylesheet" href="css/menu.css" type="text/css"/>

<html>
    <?php include "php/head.php" ?>
    </head>
    <body>
        <?php include "php/header.php" ?>
        <div class="board-menu-container">
        	<div class="board-menu-container-title
        	">
        		Menu
        	</div>
        	<hr class="board-menu-container-hr">
        	<div class="board-menu-content">
        		<ul class="board-menu-navigation">
        			<li class="board-menu-navigation-item">
                        <input type="checkbox" id="btnControl"/>
                            <label class="btn" for="btnControl">
                    				<a class="board-menu-navigation-item-link" href="#">
                    					<span>
                                            
                                        </span>
                                        À propos de ce tableau
                                        <div class="board-menu-item-description">Ajouter une description à votre tableau</div>
                    				</a>
                                <div class="board-menu-content-plus">
                                    <h4>Créé par:</h4>
                                    <hr class="board-menu-container-hr">
                                </div>
                            </label>
        			</li>
                    <li class="board-menu-navigation-item">
                        <a class="board-menu-navigation-item-link" href="#">
                            <span>
                                
                            </span>
                            Changer le fond d'écran
                        </a>
                        <div class="board-menu-content-plus">
                            <h4>Changer la couleur de fond:</h4>
                            <div class="square-background-color" alt style="background-color: rgb(0, 121, 191);"></div>
                            <div class="square-background-color" alt style="background-color: rgb(210, 144, 52);"></div>
                            <div class="square-background-color" alt style="background-color: rgb(81, 152, 57);"></div>
                            <div class="square-background-color" alt style="    background-color: rgb(176, 70, 50);"></div>
                            <div class="square-background-color" alt style="background-color: rgb(137, 96, 158);"></div>
                            <div class="square-background-color" alt style="background-color: rgb(205, 90, 145);"></div>
                            <div class="square-background-color" alt style="background-color: rgb(75, 191, 107);"></div>
                            <div class="square-background-color" alt style="background-color: rgb(0, 174, 204);"></div>
                            <div class="square-background-color" alt style="background-color: rgb(131, 140, 145);"></div>
                            <div class="square-background-color" alt style="background-color: #f4f5f7;"></div>
                            <hr class="board-menu-container-hr">
                        </div>
                    </li>
                    <li class="board-menu-navigation-item">
                        <a class="board-menu-navigation-item-link" href="#">
                            <span>
                                
                            </span>
                            Rechercher dans les cartes
                        </a>
                        <div class="board-menu-content-plus">
                            <h4>Rechercher par nom:</h4>
                            <input type="text" name="card-name">
                            <hr class="board-menu-container-hr">
                            <h4>Rechercher par étiquette:</h4>
                            <ul class="board-menu-navigation">
                                <li class="board-menu-navigation-item">
                                    <div id="carré" alt style="background-color: #b3bac5;"></div><span class="square-label">Aucune étiquette</span>
                                </li>
                                <li class="board-menu-navigation-item">
                                    <div id="carré" alt style="background-color: #61bd4f;"></div><div class="square-label">Étiquette verte</div>
                                </li>
                                <li class="board-menu-navigation-item">
                                    <div id="carré" alt style="background-color: #f2d600;"></div><div class="square-label">Étiquette jaune</div>
                                </li>
                                <li class="board-menu-navigation-item">
                                    <div id="carré" alt style="background-color: #ff9f1a;"></div><div class="square-label">Étiquette orange</div>
                                </li>
                                <li class="board-menu-navigation-item">
                                    <div id="carré" alt style="background-color: #eb5a46;"></div><div class="square-label">Étiquette rouge</div>
                                </li>
                                <li class="board-menu-navigation-item">
                                    <div id="carré" alt style="background-color: #c377e0;"></div><div class="square-label">Étiquette violet</div>
                                </li>
                                <li class="board-menu-navigation-item">
                                    <div id="carré" alt style="background-color: #0079bf;"></div><div class="square-label">Étiquette bleu</div>
                                </li>
                            </ul>
                            <hr class="board-menu-container-hr">
                            <h4>Rechercher par membres:</h4>
                            <hr class="board-menu-container-hr">
                        </div>
                    </li>
                    <li class="board-menu-navigation-item">
                        <a class="board-menu-navigation-item-link" href="#">
                            <span>
                                
                            </span>
                            Stickers
                        </a>
                        <div class="board-menu-content-plus">
                            <h4>Ajout de stickers</h4>
                            <img src="img/sticker1.png">
                            <img src="img/sticker2.png">
                            <img src="img/sticker3.png">
                            <img src="img/sticker4.png">
                            <img src="img/sticker5.png">
                            <img src="img/sticker6.png">
                            <img src="img/sticker7.png">
                            <img src="img/sticker8.png">
                            <hr class="board-menu-container-hr">
                        </div>
                    </li>
                    <li class="board-menu-navigation-item">
                        <a class="board-menu-navigation-item-link-plus" href="#">
                            Plus
                        </a>
                        <div class="board-menu-content-plus">
                            <ul class="board-menu-navigation">
                                <li class="board-menu-navigation-item">
                                    <a class="board-menu-navigation-item-link" href="#">
                                        <span>
                                            
                                        </span>
                                        Paramètres
                                    </a>
                                    <div>
                                        <h4> Modifier l'équipe...</h4>
                                        <hr class="board-menu-container-hr">
                                        <h4> Images de couverture de la carte activées</h4>
                                        <h4> Afficher les images sur les cartes</h4>
                                        <hr class="board-menu-container-hr">
                                    </div>
                                </li>
                                <li class="board-menu-navigation-item">
                                    <a class="board-menu-navigation-item-link" href="#">
                                        <span>
                                            
                                        </span>
                                        étiquettes
                                    </a>
                                </li>
                                <li class="board-menu-navigation-item">
                                    <a class="board-menu-navigation-item-link" href="#">
                                        <span>
                                            
                                        </span>
                                        éléments archivés
                                    </a>
                                </li>
                                <hr class="board-menu-container-hr">
                                <li class="board-menu-navigation-item">
                                    <a class="board-menu-navigation-item-link" href="#">
                                        <span>
                                            
                                        </span>
                                        Paramètres e-mail vers tableau
                                    </a>
                                </li>
                                <li class="board-menu-navigation-item">
                                    <a class="board-menu-navigation-item-link" href="#">
                                        <span>
                                            
                                        </span>
                                        Suivre 
                                    </a>
                                </li>
                                <li class="board-menu-navigation-item">
                                    <a class="board-menu-navigation-item-link" href="#">
                                        <span>
                                            
                                        </span>
                                        Copier le tableau
                                    </a>
                                </li>
                                <li class="board-menu-navigation-item">
                                    <a class="board-menu-navigation-item-link" href="#">
                                        <span>
                                            
                                        </span>
                                        Imprimer et exporter
                                    </a>
                                </li>
                                <hr class="board-menu-container-hr">
                                <li class="board-menu-navigation-item">
                                    <a class="board-menu-navigation-item-link" href="#">
                                        <span>
                                            
                                        </span>
                                        Quitter le tableau…
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    
        		</ul>
                <hr class="board-menu-container-hr">
                <div class="board-menu-butler">
                    <ul class="board-menu-navigation">
                        <li class="board-menu-navigation-item">
                            <a class="board-menu-navigation-item-link" href="#">
                                <span>
                                    
                                </span>
                                Butler
                                <div class="board-menu-item-description"> Automatiser les cartes et plus encore…</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <hr class="board-menu-container-hr">
                <div class="board-menu-power-ups">
                    <ul class="board-menu-navigation">
                        <li class="board-menu-navigation-item">
                            <a class="board-menu-navigation-item-link" href="#">
                                <span>
                                    
                                </span>
                                Power-Ups
                                <div class="board-menu-item-description"> Calendrier, Google Drive et plus encore…</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <hr class="board-menu-container-hr">
                <div class="board-menu-activité">
                    <ul class="board-menu-navigation">
                        <li class="board-menu-navigation-item">
                            <a class="board-menu-navigation-item-link" href="#">
                                <span>
                                    
                                </span>
                                Activité
                            </a>
                        </li>
                    </ul>
                </div>
        	</div>
        </div>
        <?php include "php/footer.php" ?>
        </footer>
    </body>
</html>




