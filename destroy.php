<html>
    <?php include 'php/head.php' ?>
    <?php include 'php/session.php' ?>
    <?php 
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (isset($_POST['valided'])) {
                $bdd->destroyUser($_SESSION['user']);
                unset($_SESSION['user']);
                echo '<script> window.location.href=\'/yep_project1_2019/login\';</script>';
            } else if (isset($_POST['canceled'])) {
                echo '<script> window.location.href=\'/yep_project1_2019/user_setting\'</script>';
            }
        }
    ?>
    </head>
    <body>
        <?php include 'php/header.php' ?>
        </header>
        <h1><?php echo _destroyTitle; ?></h1> 
        <p><?php echo _destroyText; ?></p><br />
        <form action='/yep_project1_2019/destroy' method='POST'>
            <input name='<?php echo _confirm; ?>' type='submit' value='Valider' style='color: red;' />
        </form>
        <form action='/yep_project1_2019/destroy' method='POST'>
            <input name='<?php echo _cancel; ?>' type='submit' value='Annuler' style='color: green;' />
        </form>
    </body>
</html>