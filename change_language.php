<?php
    session_start();
    include 'database/database.php';
    $bdd = new Database();
    include 'php/session.php';

    if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
        echo '<script> window.history.back() </script>';
    }
    $target = (isset($_POST['fr'])) ? 'fr' : 'en';
    $bdd->updateLanguage($_SESSION['user'], $target);
    echo '<script> window.location.href=\'/yep_project1_2019/user_setting\'</script>';
?>
