<html>
    <?php include "php/head.php" ?>
    <?php include 'php/session.php' ?>
    <?php
        $user = $bdd->getUserByEmail($_SESSION['user']);
        $swap = ($user->getLanguage() == 'fr') ? 'en' : 'fr';
    ?>
    </head>
    <body>
        <?php include "php/header.php" ?>
        <h1><?php echo _userSetting ?></h1>
        <h2><?php echo _information ?></h2><hr />
        <img src="<?php echo $user->getImage() ?>" alt="Utilisateur <?php echo $user->getPseudo() ?>" />
        <a href='/yep_project1_2019/image'><?php echo _changeProfilPicture; ?></a>
        <p><?php echo _firstName ?>: <?php echo $user->getName(); ?></p><br />
        <p><?php echo _lastName ?>: <?php echo $user->getSurname(); ?></p><br />
        <p><?php echo _loginEmail ?>: <?php echo $user->getEmail(); ?></p><br />
        <p><?php echo _pseudo ?>: <?php echo $user->getPseudo(); ?></p><br />
        <a href='/yep_project1_2019/change_information'><?php echo _changeInformations; ?></a><br />
        <a href='/yep_project1_2019/recovery'><?php echo _changePassword; ?></a><br />
        <h2><?php echo _language; ?></h2><hr />
        <p><?php echo _language; ?>: <?php echo $user->getLanguage(); ?></p>
        <form action='/yep_project1_2019/change_language' method='POST'>
            <input type='submit' value='<?php echo _changeLang . ' ' . $swap?>' name='<?php echo $swap ?>' />
        </form>
        <h2><?php echo _deleteaccount ?></h2><hr />
        <a href='/yep_project1_2019/destroy'><?php echo _closeAccount ?></a>
        <?php include "php/footer.php" ?>
        </footer>
    </body>
</html>