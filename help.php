<link rel="stylesheet" href="css/help.css" type="text/css"/>

<html>
    <?php include "php/head.php" ?>
    </head>
    <body>
        <?php include "php/header.php" ?>
        <div class='section-help'>
        	<h2><?php echo _helpTitle; ?></h2>
        	<ul class="popArticles">
        		<li>
                    <a href="help/using_markdown_in_trello">
            			<span><?php echo _helpMessage1; ?></span>
                    </a>
        		</li>
        		<li>
                    <a href="help/deleting_a_board">
        			    <span><?php echo _helpMessage2; ?></span>
                    </a>
        		</li>
        		<li>
                    <a href="help/loading_resources_from_trellos_cdn">
        	       		<span><?php echo _helpMessage3; ?></span>
                    </a>
        		</li>
        		<li>
                    <a href="help/using_trello_with_google_calendar">
              			<span><?php echo _helpMessage4; ?></span>
                    </a>
        		</li>
        		<li>
                    <a href="help/trello_gold_user_guide">
              			<span><?php echo _helpMessage5; ?></span>
                    </a>
        		</li>
        	</ul>
        	<ul class="popArticles">
        		<li>
                    <a href="help/archiving_and_deleting_cards">
            			<span><?php echo _helpMessage6; ?></span>
                    </a>
        		</li>
        		<li>
                    <a href="help/viewing_cards_in_a_calendar_view">
             			<span><?php echo _helpMessage7; ?></span>
                    </a>
        		</li>
        		<li>
                    <a href="help/slack_app">
              			<span><?php echo _helpMessage8; ?></span>
                    </a>
        		</li>
        		<li>
                    <a href="help/how_to_use_trello_like_a_pro">
        		      	<span><?php echo _helpMessage9; ?></span>
                    </a>
        		</li>
        		<li>
                    <a href="help/creating_cards_by_email">
        			    <span><?php echo _helpMessage10; ?></span>
                    </a>
        		</li>
        	</ul>
       		<hr class="sep"></hr>
        </div>
        <div>
            <a href="menu">
                <span>MENU (provisoire)</span>
            </a>
        </div>
        <?php include "php/footer.php" ?>
        </footer>
    </body>
</html>