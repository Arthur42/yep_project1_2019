<?php
    session_start();
    include __DIR__ . '/database/database.php';
    $bdd = new Database;
    if (isset($_GET["board"])) {
        if (isset($_GET["archiveColumn"])) {
            $bdd->archiveColumn($_GET["board"], $_SESSION['user'], $_GET["archiveColumn"], 1);
        }
        if (isset($_GET["oldColumnPos"]) && isset($_GET["newColumnPos"])) {
            $bdd->changeColumnPosition($_GET["board"], $_SESSION['user'], $_GET["oldColumnPos"], $_GET["newColumnPos"]);
        }
        $board_column = $bdd->getBoardColumn($_GET["board"], $_SESSION['user']);
        $board_content = $bdd->getBoardContent($_GET["board"], $_SESSION['user']);
        $data = '{"column":' . $board_column . ', "content":' . $board_content .'}';
        header('Content-Type: application/json');
        echo json_encode($data);
    }
?>